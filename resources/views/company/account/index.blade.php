@extends('layouts.app')
@section('content')
<div class="sixteen wide column">
    <h3>Mis Datos / Actualizar Informacion</h3>
    <br>
    <img src="/img/companies/{{ $user->photo}}" alt="" height="100"><br>
    Email : {{ $user->email }}
    <br>
    <br>
    <br>
    <form class="ui form" action="/account_company/update" method="post" enctype="multipart/form-data">

    {{ csrf_field() }}
    <input type="hidden" name="company_id" value="{{ $user->id}}">
        <div class="four  fields">
            <div class="field">
                <label>Nombre</label>
                <div class="ui  input">
                    <input type="text" name="first_name" value="{{ $user->first_name}}" required>
                </div>
            </div>
            <div class="field">
                <label>Apellido</label>
                <div class="ui  input">
                    <input type="text" name="last_name" value="{{ $user->last_name}}" required>
                </div>
            </div>
            <div class="field">
                <label>Email</label>
                <div class="ui  input">
                    <input type="email" name="email" value="{{ $user->email}}" required>
                </div>
            </div>
            <div class="field">
                <label>Password</label>
                <div class="ui  input">
                    <input type="password" name="password">
                </div>
            </div>

        </div>

        <div class="three  fields">
            <div class="field">
                <label>Nombre Empresa</label>
                <div class="ui  input">
                    <input type="text" name="url_name" value="{{ $user->company_name}}" required>
                </div>
            </div>
            <div class="field">
                <label>Actualizar Foto/Logo</label>
                <div class="ui  input">
                    <input type="file" name="company_photo" >
                </div>
            </div>
            <div class="field">
                <label>Vista de Plataforma</label>
                <div class="ui  input">
                    <select class="dropdown" name="private_site">
                      <option value="0" {{ $user->private_site == 0 ? 'selected="selected"':''}}>Publico</option>
                      <option value="1" {{ $user->private_site == 1 ? 'selected="selected"':''}}>Privado</option>
                    </select>
                </div>
            </div>

        </div>
        <button class="ui right floated small green labeled icon button" type="submit" data-content="Crear nuevo usuario">
            <i class="refresh icon"></i> Actualizar Mis Datos
        </button>
    </form>
</div>
@endsection
