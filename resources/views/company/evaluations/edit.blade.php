@extends('layouts.app')
@section('content')


<div class="sixteen wide column">
<h4 class="ui horizontal dividing header">Editar Evaluacion</h4>
<form class="ui form" action="/evaluations_company/{{$evaluation->id}}" method="post" name="evaluation" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="put">
    <div class="four  fields">
        <div class="field">
            <label>Titulo</label>
            <div class="ui  input">
                <input type="text" name="title" value="{{$evaluation->title}}" required>
            </div>
        </div>
        <div class="field">
            <label>Descripcion (breve)</label>
            <div class="ui  input">
                <input type="text" name="description" value="{{$evaluation->description}}" required>
            </div>
        </div>
        <div class="field">
            <label>Tipo Evaluacion</label>
            <div class="ui  input">
                <select class="dropdown" name="type" id="type" required>
                    <option value="like_dlike" {{ $evaluation->type == 'like_dlike'? 'selected="selected"':'' }}> MG / NMG</option>
                    <option value="yes_no" {{ $evaluation->type == 'yes_no'? 'selected="selected"':'' }}>Si/ No</option>
                    <option value="star_value" {{ $evaluation->type == 'star_value'? 'selected="selected"':'' }}> Valoracion</option>
                    <option value="vote" {{ $evaluation->type == 'vote'? 'selected="selected"':'' }}>Votacion o Sufragio</option>
                </select>
            </div>
        </div>
        <div class="field">
            <label>Titulo Secundario / Sufijo</label>
            <div class="ui input">
                <input type="text" name="suffix" value="{{$evaluation->suffix}}" required>
            </div>
        </div>

    </div>
    <div class="five  fields">
        <div class="field">
            <label>Fecha de Termino</label>
            <div class="input">
                <div class="ui calendar" id="date">
                  <div class="ui input left icon">
                    <i class="calendar icon"></i>
                    <input type="text" name="end_date_eval" value="{{ date("d/m/Y",strtotime($evaluation->end_date))}}">
                  </div>
                </div>
            </div>
        </div>
        <div class="field">
            <label>Modo de Evaluacion</label>
            <div class="ui  input">
                <select class="dropdown" name="mode_eva" id="mode" {{$evaluation->type == 'vote' ? 'disabled':''}} required>
                    <option value="single" id="single" {{ $evaluation->mode == 'single' ? 'selected="selected"':'' }}>Single / Unico</option>
                    <option value="multiple" id="multiple" {{ $evaluation->mode == 'multiple' ? 'selected="selected"':'' }}>Multiple / Competencia</option>
                </select>
            </div>
        </div>

        <div class="field">
            <label>Puntaje por Evaluar</label>
            <div class="ui  input">
                <input type="number" name="score" value="{{$evaluation->score}}" required>
            </div>
        </div>

        <div class="field">
            <label>Estado</label>
            <div class="ui  input">
                <select class="dropdown" name="status" required>
                    <option value="enabled" {{ $evaluation->status == 'enabled' ? 'selected="selected"':'' }}>Activo</option>
                    <option value="disabled" {{ $evaluation->status == 'disabled' ? 'selected="selected"':'' }}>Inactivo</option>
                </select>
            </div>
        </div>
        <div class="field">
            <label>Visualizacion</label>
            <div class="ui  input">
                <select class="dropdown" name="auth_view" required>
                    <option value="disabled" {{ $evaluation->auth_view == 'disabled' ? 'selected="selected"':'' }}>Publica (Todos)</option>
                    <option value="enabled" {{ $evaluation->auth_view == 'enabled' ? 'selected="selected"':'' }}>Privada (Solo Usuarios Logeados)</option>
                </select>
            </div>
        </div>

    </div>

    <br>
    <button class="ui right floated small green labeled icon button" type="submit" data-content="Crear nuevo usuario">
        <i class="refresh icon"></i> Actualizar
    </button>
</form>


</div>

<script type="text/javascript">
$( "#count_items" ).change(function() {
    var count = $(".item_factor").length;
    var cant_items = $('#count_items :selected').val();
    if(count < cant_items){
        for(count; count < cant_items; count++){
            $('#items').append( '<input type="text"  class="item_factor" name="item_'+(count+1)+'" placeholder="Ingrese Factor '+(count+1)+'" style="margin: 5px;" required>');
          }
    }

    if(count > cant_items){
      var del = count - cant_items
        $('#items > .item_factor').slice(-del).remove();
    }
});

$( "#type" ).change(function() {

    var type = $('#type :selected').val();
    if(type == 'vote')
    {
        $('#mode').parent().addClass("disabled");

    }
    else
    {
        $('#mode').parent().removeClass("disabled");
        $('#mode').prop("disabled", false);
    }
});


</script>
@endsection
