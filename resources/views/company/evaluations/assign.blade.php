@extends('layouts.app')
@section('content')

<div class="sixteen wide column">
<h5>Asignacion de Fichas para :
<div class="ui large label">
    <i class="text file icon"></i>
     Evaluacion
     <a class="detail">{{$evaluation->title}}</a>
</div>
</h5>

<hr>
<span><span class="ui red label">Instrucciones</span> Haz clic en las fotografias para seleccionar las fichas que se asociaran a la Evaluacion y luego al boton verde "Asignar"</span><br>
<hr>

@if($evaluation->mode == 'single')<span class="ui blue label"> <i class="user icon"></i>Seleccion Unica</span>@endif
@if($evaluation->mode == 'multiple')<span class="ui blue label"> <i class="users icon"></i>Seleccion Multiple</span>@endif
<br><br>
<form class="ui form" action="/profiles_assign" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="evaluation_id" value="{{$evaluation->id}}">
    <button class="ui small green labeled icon button" type="submit" data-content="Enviar Fichas">
        <i class="plus icon"></i> Asignar
    </button>
<br>
<br>
    @if($evaluation->mode == 'single')
        <select  class="image-picker show-html" name="profiles[]">
    @else
        <select multiple="multiple" class="image-picker show-html" name="profiles[]">
    @endif


        @forelse($profiles_assign as $profile_a)
        <option data-img-src="{{$profile_a->photo}}" value="{{$profile_a->id}}" selected="selected">{{substr($profile_a->title,0,16)}}..</option>
        @empty
        @endforelse
        @forelse($profiles_no_assign as $profile_n)
        <option data-img-src="{{$profile_n->photo}}" value="{{$profile_n->id}}" >{{substr($profile_n->title,0,16)}}..</option>
        @empty
        @endforelse
    </select>


</form>
</div>
<script type="text/javascript">
$(".image-picker").imagepicker({
     hide_select : true,
     show_label  : true
   })
</script>
@endsection
