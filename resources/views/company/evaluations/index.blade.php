@extends('layouts.app')
@section('content')


<div class="sixteen wide column">
<h4 class="ui horizontal dividing header">Crear Evaluacion</h4>
<form class="ui form" action="/evaluations_company" method="post" name="evaluation" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="five  fields">
        <div class="field">
            <label>Titulo</label>
            <div class="ui  input">
                <input type="text" name="title" required>
            </div>
        </div>
        <div class="field">
            <label>Descripcion (breve)</label>
            <div class="ui  input">
                <input type="text" name="description" required>
            </div>
        </div>
        <div class="field">
            <label>Tipo Evaluacion</label>
            <div class="ui  input">
                <select class="dropdown" name="type" id="type" required>
                    <option value="like_dlike"> MG / NMG</option>
                    <option value="yes_no">Si/ No</option>
                    <option value="star_value"> Valoracion</option>
                    <option value="vote">Votacion o Sufragio</option>
                </select>
            </div>
        </div>
        <div class="field">
            <label>Cantidad de Factores</label>
            <div class="ui  input">
              <select class="dropdown" id="count_items" name="count_items" required>
                  <option value="1" selected="selected">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
              </select>
            </div>
        </div>
        <div class="field">
            <label>Titulo Secundario / Sufijo</label>
            <div class="ui input">
                <input type="text" name="suffix" required>
            </div>
        </div>

    </div>
    <div class="five  fields">
        <div class="field">
            <label>Fecha/Hora de Termino</label>
            <div class="input">
                <div class="ui calendar" id="date">
                  <div class="ui input left icon">
                    <i class="calendar icon"></i>
                    <input type="text" name="end_date_eval" placeholder="">
                  </div>
                </div>
            </div>
        </div>
        <div class="field">
            <label>Modo de Evaluacion</label>
            <div class="ui  input">
                <select class="dropdown" name="mode_eva" id="mode" required>
                    <option value="single" id="single" >Single / Unico</option>
                    <option value="multiple" id="multiple">Multiple / Competencia</option>
                </select>
            </div>
        </div>

        <div class="field">
            <label>Puntaje por Evaluar</label>
            <div class="ui  input">
                <input type="number" name="score" required>
            </div>
        </div>

        <div class="field">
            <label>Estado</label>
            <div class="ui  input">
                <select class="dropdown" name="status" required>
                    <option value="enabled">Activo</option>
                    <option value="disabled">Inactivo</option>
                </select>
            </div>
        </div>
        <div class="field">
            <label>Visualizacion</label>
            <div class="ui  input">
                <select class="dropdown" name="auth_view" required>
                    <option value="disabled">Publica (Todos)</option>
                    <option value="enabled">Privada (Solo Usuarios Logeados)</option>
                </select>
            </div>
        </div>

    </div>

    <div class="column">
        <div class="field">
            <label>Etiquetas/Tags (Busca o Escribe separando por comas)</label>
            <select name="tags[]" multiple="multiple" class="ui search fluid dropdown">
              <option value="">Tags</option>
              @forelse($tags as $tag)
              <option value="{{ $tag->name }}">{{ $tag->name }}</option>
              @empty
              @endforelse
            </select>
        </div>
    </div>
    <br>
    <span id="item_fators">
    <h4 class="ui horizontal dividing  header">Factores / Descriptores / Preguntas</h4>
    <div class="nine wide column">
      <div id="items">
         <input class="item_factor" type="text" name="item_1" id="item_1" placeholder="Ingrese Factor 1"  style="margin: 5px;" required>
      </div>
    </div>
    </span>



    <br>
    <button class="ui right floated small green labeled icon button" type="submit" data-content="Crear nuevo usuario">
        <i class="plus icon"></i> Crear
    </button>
</form>
<h4 class="ui horizontal dividing  header">Lista</h4>
<table class="ui celled table sortable">
    <thead>
        <tr>
            <th>Titulo</th>
            <th>Tipo de Evaluacion</th>
            <th><i class="checked calendar icon"></i> Termino</th>
            <th>Modo</th>
            <th><i class="unhide icon"></i></i></th>
            <th>Estado</th>
            <th>Fichas Asignadas</th>
            <th>Factores / Descriptores</th>
            <th> Acciones Edit/Delete</th>
        </tr>
    </thead>
    <tbody>
        @forelse($evaluations as $evaluation)
        <tr>
            <td><small>{{ $evaluation->title }}</small></td>
            <td>
                @if($evaluation->type == 'like_dlike')<span href="" class="tooltip" data-content="Evaluacion ME GUSTA / NO ME GUSTA"><i class="thumbs up icon"></i><i class="thumbs down icon"></i></span> @endif
                @if($evaluation->type == 'yes_no') <span href="" class="tooltip" data-content="Evaluacion SI o NO">SI / NO</span> @endif
                @if($evaluation->type == 'star_value') <span href="" class="tooltip" data-content="Evaluacion de VALORACION 1-5">
                    <i class="star icon"></i><i class="star icon"></i><i class="star icon"></i><i class="star icon"></i><i class="star icon"></i></span> @endif
                @if($evaluation->type == 'vote') <span href="" class="tooltip" data-content="Evaluacion de VOTACION"><i class="hand paper icon"></i><i class="hand pointer icon"></i></span> @endif
            </td>
            <td>{{ date("d/m/Y H:i",strtotime($evaluation->end_date)) }}</td>
            <td>
              @if($evaluation->mode == 'single')
              <span class="tooltip mini ui  icon green button" data-content="Modo Single / Unico"><i class="user icon"></i></span>
              @else
              <span class="tooltip mini ui  icon orange button" data-content="Modo Multiple / Competencia"><i class="users icon"></i></span>
              @endif
              </td>
            <td>
              @if($evaluation->auth_view == 'disabled')
              <span class="tooltip mini ui  icon blue button" data-content="Visualizacion Publica"><i class="unlock icon"></i></span>
              @else
              <span class="tooltip mini ui  icon red button" data-content="Visualizacion Privada (necesita Login)"><i class="lock icon"></i></span>
              @endif


            </td>
            <td>
              @if($evaluation->status == 'enabled')
              <a class="tooltip mini ui  icon green button" href="#"  data-content="Activo" >
                  <i class="check circle icon"></i>
              </a>

              @else
              <a class="tooltip mini ui  icon red button" href="#"  data-content="Inactivo" >
                  <i class="minus circle icon"></i>
              </a>

              @endif


            <td>
            @forelse($evaluation->profiles as $profile)
                <img src="{{ $profile->photo}}" alt="" height="40">
            @empty
                -
            @endforelse

            <a class="tooltip mini ui float right  icon blue button" href="/profile_assign/{{$evaluation->id}}"  data-content="Asignar Ficha/Fichas" style="float:right;">
                <i class="edit icon"></i> Editar
            </a>
            </td>
            <td>
            @if($evaluation->type != 'vote')
              @forelse($evaluation->items as $item)
                  <small>{{$item->description}}</small> ,<br>
              @empty
              @endforelse
              <a class="tooltip mini ui float right  icon blue button" href="/eva_items/{{$evaluation->id}}"  data-content="Editar Factores / Descriptores" style="float:right;">
                  <i class="edit icon"></i> Editar
              </a>
             @else
             N/A
             @endif
            </td>

            <td>
                <a class="circular mini ui icon blue button info-modal-link" href="/evaluations_company/{{$evaluation->id}}/edit"  title="Editar" style="float:left;">
                    <i class="icon  edit"></i>
                </a>
                <form action="/evaluations_company/{{ $evaluation->id }}" method="post" onSubmit="if(!confirm('Estas seguro de eliminar la evaluacion!?')){return false;}" >
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class="circular mini ui icon red button" type="submit" title="Eliminar" style="float:left;">
                        <i class="icon  remove"></i>
                    </button>
                </form>


            </td>
        </tr>
        @empty
        <span>sin registros aun</span>
        @endforelse
    </tbody>
</table>

</div>

<script type="text/javascript">
$( "#count_items" ).change(function() {
    var count = $(".item_factor").length;
    var cant_items = $('#count_items :selected').val();
    if(count < cant_items){
        for(count; count < cant_items; count++){
            $('#items').append( '<input type="text"  class="item_factor" name="item_'+(count+1)+'" placeholder="Ingrese Factor '+(count+1)+'" style="margin: 5px;" required>');
          }
    }

    if(count > cant_items){
      var del = count - cant_items
        $('#items > .item_factor').slice(-del).remove();
    }
});

$( "#type" ).change(function() {

    var type = $('#type :selected').val();
    if(type == 'vote')
    {
        $('#mode').parent().addClass("disabled");
        $('#count_items').parent().addClass("disabled");

        $("#items input").each(function(){
       		    $(this).removeAttr("required");
       		});
        $('#item_fators').css('display','none');
    }
    else
    {
        $('#mode').parent().removeClass("disabled");
        $('#count_items').parent().removeClass("disabled");

        $("#items input").each(function(){
                $(this).prop("required", true);
            });
        $('#item_fators').css('display','block');
    }


// $('#myTest').prop("required", true);

// $('#myTest').removeAttr("required");

});
$('.ui.dropdown').dropdown({
  allowAdditions: true,
});

</script>
@endsection
