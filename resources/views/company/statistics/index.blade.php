@extends('layouts.app')
@section('content')
<div class="sixteen wide column">

<h3>Estadisticas</h3>
<div class="ui statistics">

  <div class="statistic">
    <div class="value">
      <i class="users blue icon"></i> {{ $users->count()}}
    </div>
    <div class="label">
      Usuarios
    </div>
  </div>

  <div class="statistic">
    <div class="value">
      <i class="browser  green icon"></i> {{ $profiles->count()}}
    </div>
    <div class="label">
      Fichas
    </div>
  </div>

  <div class="statistic">
    <div class="value">
      <i class="bar chart red icon"></i> {{ $evaluations->count()}}
    </div>
    <div class="label">
      Evaluaciones
    </div>
  </div>

</div>

<h3>Alertas</h3>


</div>
@endsection
