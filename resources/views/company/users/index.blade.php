@extends('layouts.app')
@section('content')
<div class="sixteen wide column">
<h3>Mis Usuarios</h3>
<h4 class="ui horizontal dividing header">Crear Nuevo</h4>
<form class="ui form" action="/users_company" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="four  fields">
        <div class="field">
            <label>Nombre</label>
            <div class="ui mini input">
                <input type="text" name="first_name" placeholder="Ingrese Nombre" required>
            </div>
        </div>
        <div class="field">
            <label>Apellido</label>
            <div class="ui mini input">
                <input type="text" name="last_name" placeholder="Ingrese Apellido" required>
            </div>
        </div>
        <div class="field">
            <label>Email</label>
            <div class="ui mini input">
                <input type="email" name="email" placeholder="Ingrese Email" required>
            </div>
        </div>
        <div class="field">
            <label>Password</label>
            <div class="ui mini input">
                <input type="password" name="password" placeholder="Ingrese Password" required>
            </div>
        </div>
    </div>

    <button class="ui right floated small green labeled icon button" type="submit" data-content="Crear nuevo usuario">
        <i class="add user icon"></i> Crear
    </button>
</form>
<h4 class="ui horizontal dividing  header">Lista</h4>
<table class="ui celled table sortable">
    <thead>
        <tr>
            <th>#ID</th>
            <th>Fecha Registro</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Email</th>
            <th>Genero</th>
            <th>Estado</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @forelse($users as $user)
        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->created_at }}</td>
            <td>{{ $user->first_name }}</td>
            <td>{{ $user->last_name }}</td>
            <td>{{ $user->email }}</td>
            <td>
                @if($user->gender == 'm')
                Masculino
                @endif
                @if($user->gender == 'f')
                Femenino
                @endif
            </td>
            <td>
                @if($user->status == 'active')
                <i class="fa fa-check-circle-o" style="color:green;"></i> Activo
                @endif
                @if($user->status == 'inactive')
                <i class="fa fa-ban" style="color:red;" ></i> Inactivo
                @endif

                <form  action="/users_company/{{ $user->id }}/change_status" method="get">
                    <button class="mini ui icon defaut button" type="submit" title="Cambiar estado">
                        <i class="icon blue refresh"></i>
                    </button>
                </form>
            </td>

            <td>
                <a class="circular mini ui icon defaut button info-modal-link" href="/users_company/{{$user->id}}/edit"  title="Editar" style="float:left;">
                    <i class="icon  blue edit"></i>
                </a>
                <form action="/users_company/{{ $user->id }}/delete" method="post" onSubmit="if(!confirm('Estas seguro de eliminar al usuario!?')){return false;}" >
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class="circular mini ui icon defaut button" type="submit" title="Eliminar" style="float:left;">
                        <i class="icon red remove user"></i>
                    </button>
                </form>
            </td>
        </tr>
        @empty
        <span>sin registros aun</span>
        @endforelse
    </tbody>
</table>

</div>
@endsection
