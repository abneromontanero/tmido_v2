@extends('layouts.app')
@section('content')
<div class="sixteen wide column">
<h4 class="ui horizontal dividing header">Editar Usuario</h4>
<form class="ui form" action="/users_company/update" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="user_id" value="{{$user->id}}">
    <div class="four  fields">
        <div class="field">
            <label>Nombre</label>
            <div class="ui mini input">
                <input type="text" name="first_name" placeholder="Ingrese Nombre" value="{{$user->first_name}}" required>
            </div>
        </div>
        <div class="field">
            <label>Apellido</label>
            <div class="ui mini input">
                <input type="text" name="last_name" placeholder="Ingrese Apellido"  value="{{$user->last_name}}" required>
            </div>
        </div>
        <div class="field">
            <label>Email</label>
            <div class="ui mini input">
                <input type="email" name="email" placeholder="Ingrese Email"  value="{{$user->email}}" required>
            </div>
        </div>
        <div class="field">
            <label>Password</label>
            <div class="ui mini input">
                <input type="password" name="password" placeholder="Ingrese Password" >
            </div>
        </div>
    </div>

    <button class="ui right floated small green labeled icon button" type="submit" data-content="Actualizar">
        <i class="user icon"></i> Actualizar
    </button>
</form>
</div>
@endsection
