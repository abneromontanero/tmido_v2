@extends('layouts.app')
@section('content')


<div class="sixteen wide column">
<h4 class="ui horizontal dividing header">Editar Items / Descriptores / Factores</h4>

<a href="/evaluations_company">Volver</a>

    <form class="ui form" action="/update_items/{{ $item->id }}" method="post"  enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="evaluation_id" value="{{$item->evaluation->id}}">
        <div class="one  fields">
            <div class="field">
                <label>Descripcion</label>
                <div class="ui  input">
                    <input type="text" name="description"  value="{{ $item->description }}"required>
                </div>
            </div>

        </div>

        <button class="ui right floated small green labeled icon button" type="submit" data-content="Crear nuevo usuario">
            <i class="refresh icon"></i> Actualizar
        </button>
    </form>

</div>

@endsection
