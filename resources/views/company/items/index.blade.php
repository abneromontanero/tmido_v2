@extends('layouts.app')
@section('content')


<div class="sixteen wide column">
<h4 class="ui horizontal dividing header">Gestionar Items / Descriptores / Factores</h4>

Evaluacion: {{ $evaluation->title}}
<br>
<a class=" mini ui icon red button" href="/evaluations_company">Volver</a>

@if($items->count() < 5)
<form class="ui form" action="/eva_items/{{ $evaluation->id }}" method="post"  enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="one  field">
        <div class="field">
            <label>Descripcion</label>
            <div class="ui  input">
                <input type="text" name="description" required>
            </div>
        </div>

    </div>

    <button class="ui right floated small green labeled icon button" type="submit" data-content="Crear nuevo usuario">
        <i class="plus icon"></i> Crear
    </button>
</form>
@endif
<h4 class="ui horizontal dividing  header">Lista</h4>
<table class="ui celled table sortable">
    <thead>
        <tr>
            <th>Id</th>
            <th>Descripcion</th>
            <th>Acciones</th>

        </tr>
    </thead>
    <tbody>
        @forelse($items as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->description }}</td>
            <td>
                <a class="circular mini ui icon blue button info-modal-link" href="/eva_items/{{$item->id}}/edit"  title="Editar" style="float:left;">
                    <i class="icon   edit"></i>
                </a>
                <form action="/del_items/{{ $item->id }}" method="post" onSubmit="if(!confirm('Estas seguro de eliminar el Item / Descriptor!?')){return false;}" >
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class="circular mini ui icon red button" type="submit" title="Eliminar" style="float:left;">
                        <i class="icon  remove"></i>
                    </button>
                </form>

            </td>
        </tr>
        @empty
        <span>sin registros aun</span>
        @endforelse
    </tbody>
</div>

@endsection
