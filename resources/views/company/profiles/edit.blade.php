@extends('layouts.app')
@section('content')
<div class="sixteen wide column">
<h3>Editar Ficha</h3>
<form class="ui form" action="/profiles_company/{{$profile->id}}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_method" value="put">
    {{ csrf_field() }}

    <div class="ui grid">
        <div class="ten wide column">

                <div class="field">
                    <label>Titulo *</label>
                    <div class="ui input">
                        <input type="text" name="title" maxlength="50" value="{{$profile->title}}" required>
                    </div>
                </div>

                <div class="field">
                    <label>Descripcion (breve) *</label>
                    <div class="ui  input">
                        <input type="text" name="description" value="{{$profile->description}}" required>
                    </div>
                </div>

                <div class="field">
                    <label>URL Documento</label>
                    <div class="ui  input">
                        <input type="text" name="url_document" value="{{$profile->url_document}}" >
                    </div>
                </div>


                <div class="field">
                    <label>URL Video</label>
                    <div class="ui  input">
                        <input type="text" name="url_video" value="{{$profile->url_video}}" >
                    </div>
                </div>
                <button class="ui right floated small green labeled icon button" type="submit" data-content="Crear nuevo usuario">
                    <i class="refresh icon"></i> Actualizar
                </button>
        </div>
        <div class="six wide column">
            <label><strong>Foto / Logo    ** NOTA: Solo si quieres cambiar imagen</strong></label><br>

            <div class="img-container">
                <img src="" alt="Editor de Imagen">
            </div>

                <textarea style="display:none;" id="imagen" name="photo" rows="8" cols="80" required>{{$profile->photo}}</textarea>

            <div class="row" id="actions">
              <div class=" docs-buttons">
                <div class=" ui small blue icon buttons btn-group">
                  <button type="button" class="tooltip ui button" data-method="setDragMode" data-option="move" data-content="Activar Mover Imagen" title="Move">
                    <span class="docs-tooltip" data-toggle="tooltip" title="cropper.setDragMode(&quot;move&quot;)">
                      <span class="fa fa-arrows"></span>
                    </span>
                  </button>
                  <button type="button" class="tooltip ui blue button" data-method="setDragMode" data-option="crop" data-content="Cortar" title="Crop">
                    <span class="docs-tooltip" data-toggle="tooltip" title="cropper.setDragMode(&quot;crop&quot;)">
                      <span class="fa fa-crop"></span>
                    </span>
                  </button>
                </div>

                <div class="ui small blue icon buttons btn-group">
                  <button type="button" class="tooltip ui blue button" data-method="zoom" data-option="0.1" data-content="Acercar" title="Zoom In">
                    <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(0.1)">
                      <span class="fa fa-search-plus"></span>
                    </span>
                  </button>
                  <button type="button" class="tooltip ui blue button" data-method="zoom" data-option="-0.1" data-content="Alejar" title="Zoom Out">
                    <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(-0.1)">
                      <span class="fa fa-search-minus"></span>
                    </span>
                  </button>
                </div>

                <div class="ui small blue icon buttons btn-group">
                  <button type="button" class="tooltip ui blue button" data-method="rotate" data-option="-45" data-content="Rotar Izquerda" title="Rotate Left">
                    <span class="docs-tooltip" data-toggle="tooltip" title="cropper.rotate(-45)">
                      <span class="fa fa-rotate-left"></span>
                    </span>
                  </button>
                  <button type="button" class="tooltip ui blue button" data-method="rotate" data-option="45" data-content="Rotar Derecha" title="Rotate Right">
                    <span class="docs-tooltip" data-toggle="tooltip" title="cropper.rotate(45)">
                      <span class="fa fa-rotate-right"></span>
                    </span>
                  </button>
                </div>


                <div class="ui small  icon buttons btn-group">
                  <button type="button" class="tooltip ui blue button" data-method="reset" title="Reset" data-content="Reestablecer Imagen">
                    <span class="docs-tooltip" data-toggle="tooltip" title="cropper.reset()">
                      <span class="fa fa-refresh"></span>
                    </span>
                  </button>
                  <label class="tooltip ui green mini button btn-upload" for="inputImage" title="Upload image file" data-content="Cargar Imagen">
                    <input type="file" class="sr-only" id="inputImage" name="file" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff" style="display:none;">
                    <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs">
                     <i class="fa fa-upload" aria-hidden="true"></i>  Subir Imagen
                    </span>
                  </label>
                </div>

                <div class="btn-group btn-group-crop">
                  <button style="display:none;" type="button" class="btn btn-primary" id="btn_cortar" data-method="getCroppedCanvas">
                    <span class="docs-tooltip" data-toggle="tooltip" title="cropper.getCroppedCanvas()">
                      Generar Imagen
                    </span>
                  </button>
                </div>


                <div class="modal fade docs-cropped" id="getCroppedCanvasModal" role="dialog" aria-hidden="true" aria-labelledby="getCroppedCanvasTitle" tabindex="-1" style="display:none;">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="getCroppedCanvasTitle">Cropped</h4>
                      </div>
                      <div class="modal-body"></div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <a class="btn btn-primary" id="download" href="javascript:void(0);" download="cropped.jpg">Download</a>
                      </div>
                    </div>
                  </div>
                </div><!-- /.modal -->

            </div><!-- /.docs-buttons --><br><br>
              <img src="{{$profile->photo}}" id="imagen_prev" height="100" alt="Preview Final Imagen">
            </div>
        </div>
    </div>




</form>
</div>
<script src="/js/main.js"></script>
@endsection
