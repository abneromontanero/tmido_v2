@extends('layouts.app')
@section('content')
<div class="ui client_ container">
    <div class="ui segment " style="background-color:#1B1C1D;color:white;">
        <div class="ui inverted secondary menu">
            <a href="" style="">
                @if($user->photo)
                <img class="logo_company ui rounded image" src="/img/companies/{{ $company->photo}}" style="max-height:55px;">
                @else
                <i class="huge user circle icon"></i>
                @endif
            </a>
            <span style="font-size:13px;font-weight:bold;line-height: 60px;margin-left: 30px;">{{ $user->first_name }} {{ $user->last_name }}</span>
        </div>
    </div>
    <div class="ui segment">
        <h3 class="ui header">
            <i class="user icon"></i>
          <div class="content">
            Datos
          </div>
        </h3>
        <span class="ui blue label large"><strong>{{ $user->email }}</strong></span>
        <br><br>

        <form class="ui form" action="/user_account" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="user_id" value="{{ $user->id }}">



            <div class="four fields">
                <div class="field">
                  <label>Nombre Completo</label>
                  <input type="text" value="{{ $user->first_name }}" name="first_name" required>
                </div>
                <div class="field">
                    <label>Genero</label>
                    <select class="dropdown" name="gender" required>
                        <option value="m" {{ $user->gender == 'm'? 'selected="selected"':'' }}>Masculino</option>
                        <option value="f" {{ $user->gender == 'f'? 'selected="selected"':'' }}>Femenino</option>
                    </select>
                </div>
                <div class="field">
                <label style="float:left;">Fecha de Nacimiento</label>
                    <div class="ui calendar" id="date">
                      <div class="ui  input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" name="birth" value="{{ $user->birthdate }}" required>
                      </div>
                    </div>
                </div>
                  <div class="field">
                      <label>Comuna</label>
                      <select class="ui search dropdown" name="city_id" required>
                          @forelse($cities as $city)
                          <option value="{{ $city->id }}" {{ $user->city_id == $city->id ? 'selected="selected"':'' }}>{{ $city->label }}</option>
                          @empty
                          @endforelse
                      </select>
                  </div>
              </div>
              <button class="ui button green" type="submit"><i class="refresh icon"></i> Actualizar</button>
        </form>
    </div>
    <div class="ui segment">
        <h3 class="ui header">
            <i class="hand pointer icon"></i>
          <div class="content">
            Participacion
          </div>
        </h3>

        <span class="ui label"><strong> Tienes {{ $user->getSumScore() }} puntos</strong>  </span> <span class="ui blue label"><strong>{{ $user->getSumParticipation() }} Evaluaciones</strong>  </span>
        <br><br>
        <div class="ui divider">

        </div>
        <h3 class="ui header">
          <div class="content">
            Detalle
          </div>
        </h3>
        <table class="ui table">
            <thead>
                <tr>
                    <th>Evaluacion</th>
                    <th>Tipo</th>
                    <th>Fecha</th>
                    <th>Factor</th>
                    <th>Nota / Eleccion </th>
                </tr>

            </thead>
            <tbody>
                @forelse($user->responses->reverse() as $response)
                <tr>
                    <td>{{ $response->evaluation->title }}</td>
                    <td>
                        @if($response->evaluation->type == 'like_dlike') <i class="fa fa-thumbs-up" style="color: rgb(33, 186, 69);"></i>
                        <i class="fa fa-thumbs-down" style="color: rgb(219, 40, 40);"></i>@endif
                        @if($response->evaluation->type == 'star_value') <div class="ui star rating"><i class="icon active"></i><i class="icon active"></i>
                            <i class="icon active"></i><i class="icon active"></i><i class="icon active"></i></div> @endif
                        @if($response->evaluation->type == 'yes_no') SI / NO @endif
                        @if($response->evaluation->type == 'vote') Votacion <i class="hand paper icon"></i><i class="hand pointer icon"></i>  @endif

                    </td>
                    <td>{{ $response->created_at }}</td>
                    <td>
                        @if($response->item)
                        <span class="ui blue circular label" >{{ $response->letter }} </span> {{ $response->item->description }}
                        @else
                         -
                        @endif
                    </td>
                    <td>
                        @if($response->value == 'like')  <i class="fa fa-thumbs-up" style="color: rgb(33, 186, 69);"></i>  @endif
                        @if($response->value == 'dlike')  <i class="fa fa-thumbs-down" style="color: rgb(219, 40, 40);"></i>  @endif
                        @if($response->value == 'yes')  <strong>SI</strong>  @endif
                        @if($response->value == 'no')  <strong>NO</strong>  @endif
                        @if($response->evaluation->type == 'star_value') <strong>{{ $response->value }}</strong> <div class="ui star rating"><i class="icon active"></i></div>  @endif
                        @if($response->evaluation->type == 'vote')

                            <img class="ui mini rounded image" src="{{ $response->profile->photo }}" height="40"> {{ $response->profile->title }}

                         @endif

                    </td>
                </tr>
                @empty
                No existen datos!
                @endforelse
            </tbody>

        </table>
    </div>


        @include('layouts.footer')
</div>
@endsection
