@extends('layouts.app')
@section('content')
<div class="eleven wide column">
<h3>Empresas (Usuario)</h3>
<h4 class="ui horizontal dividing header">Crear Nuevo</h4>
<form class="ui form" action="/companies_admin" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="four  fields">
        <div class="field">
            <label>Nombre</label>
            <div class="ui mini input">
                <input type="text" name="first_name" placeholder="Ingrese Nombre" required>
            </div>
        </div>
        <div class="field">
            <label>Apellido</label>
            <div class="ui mini input">
                <input type="text" name="last_name" placeholder="Ingrese Apellido" required>
            </div>
        </div>
        <div class="field">
            <label>Email</label>
            <div class="ui mini input">
                <input type="email" name="email" placeholder="Ingrese Email" required>
            </div>
        </div>
        <div class="field">
            <label>Password</label>
            <div class="ui mini input">
                <input type="password" name="password" placeholder="Ingrese Password" required>
            </div>
        </div>
    </div>
    <div class="three  fields">
        <div class="field">
            <label>Nombre Empresa</label>
            <div class="ui mini input">
                <input type="text" name="url_name" placeholder="Ingrese Nombre" required>
            </div>
        </div>
        <div class="field">
            <label>Foto/Logo</label>
            <div class="ui mini input">
                <input type="file" name="company_photo" >
            </div>
        </div>
        <div class="field">
            <label>Vista de Plataforma</label>
            <div class="ui mini input">
                <select class="" name="private_site">
                  <option value="0">Publico</option>
                  <option value="1">Privado</option>
                </select>
            </div>
        </div>

    </div>
    <button class="ui right floated small green labeled icon button" type="submit" data-content="Crear nuevo usuario">
        <i class="add user icon"></i> Crear Empresa
    </button>
</form>
<h4 class="ui horizontal dividing  header">Lista</h4>
<table class="ui celled table sortable">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Email</th>
            <th>Nombre Empresa (URL)</th>
            <th>Foto/Logo</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @forelse($companies as $company)
        <tr>
            <td>{{ $company->first_name }}</td>
            <td>{{ $company->last_name }}</td>
            <td>{{ $company->email }}</td>
            <td>{{ $company->url_name }}</td>
            <td>
                @if($company->photo)
                <img src="/img/companies/{{ $company->photo }}" alt="" height="50">
                @else
                <small>Sin Logo</small>
                @endif
            </td>
            <td>
                <a class="circular mini ui icon defaut button info-modal-link" href="/companies_admin/{{$company->id}}/edit"  title="Editar" style="float:left;">
                    <i class="icon  blue edit"></i>
                </a>
                <form action="/companies_admin/{{ $company->id }}" method="post" onSubmit="if(!confirm('Estas seguro de eliminar a la empresa!?')){return false;}" >
                    <input type="hidden" name="_method" value="delete">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button class="circular mini ui icon defaut button" type="submit" title="Eliminar" style="float:left;">
                        <i class="icon red remove user"></i>
                    </button>
                </form>
            </td>
        </tr>
        @empty
        <span>sin registros aun</span>
        @endforelse
    </tbody>
</table>

</div>
@endsection
