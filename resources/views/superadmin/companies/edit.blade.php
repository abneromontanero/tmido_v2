@extends('layouts.app')
@section('content')
<div class="eleven wide column">
<h4 class="ui horizontal dividing header">Editar Empresa</h4>
<form class="ui form" action="/companies_admin/update" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="company_id" value="{{ $company->id}}">
    <div class="four  fields">
        <div class="field">
            <label>Nombre</label>
            <div class="ui mini input">
                <input type="text" name="first_name" value="{{ $company->first_name}}" required>
            </div>
        </div>
        <div class="field">
            <label>Apellido</label>
            <div class="ui mini input">
                <input type="text" name="last_name" value="{{ $company->last_name}}" required>
            </div>
        </div>
        <div class="field">
            <label>Email</label>
            <div class="ui mini input">
                <input type="email" name="email" value="{{ $company->email}}" required>
            </div>
        </div>
        <div class="field">
            <label>Password</label>
            <div class="ui mini input">
                <input type="password" name="password">
            </div>
        </div>
    </div>
    <div class="three  fields">
        <div class="field">
            <label>Nombre Empresa</label>
            <div class="ui mini input">
                <input type="text" name="url_name" value="{{ $company->company_name}}" required>
            </div>
        </div>
        <div class="field">
            <img src="/img/companies/{{ $company->photo}}" alt="" height="50"><br>
            <label>Foto/Logo</label>
            <div class="ui mini input">
                <input type="file" name="company_photo" >
            </div>
        </div>
        <div class="field">
            <label>Vista de Plataforma</label>
            <div class="ui mini input">
                <select class="" name="private_site">
                  <option value="0">Publico</option>
                  <option value="1">Privado</option>
                </select>
            </div>
        </div>

    </div>
    <button class="ui right floated small green labeled icon button" type="submit" data-content="Crear nuevo usuario">
        <i class="add user icon"></i> Actualizar Empresa
    </button>
</form>
</div>
@endsection
