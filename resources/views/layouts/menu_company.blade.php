<div class=" sixteen wide column">
    <div class="ui  inverted  menu">
      <a href="/statistics_company" class="{{ $page == 'statistics_company' ?  'item active' : 'item'}}">
         Estadisticas & Alertas
      </a>
      <a  href="/users_company" class="{{ $page == 'users_company' ?  'item active' : 'item'}}">
        Mis Usuarios
      </a>
      <a href="/profiles_company" class="{{ $page == 'profiles_company' ?  'item active' : 'item'}}">
        Fichas
      </a>
      <a href="/evaluations_company" class="{{ $page == 'evaluations_company' ?  'item active' : 'item'}}">
        Evaluaciones
      </a>
      <!--<a href="/comments_company" class="{{ $page == 'comments_company' ?  'item active' : 'item'}}">
        Comentarios
    </a>-->
      <a href="/account_company" class="{{ $page == 'account_company' ?  'item active' : 'item'}}">
        Mis Datos / Cuenta
      </a>
    </div>
</div>
