<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--METADATAS-->
    @if(isset($eval))
    <!-- Place this data between the <head> tags of your website -->
    <title>T-Mido | {{ $eval->title }} </title>
    <meta name="title" content="{{ $eval->title }}"/>
    <meta name="description" content="{{ $eval->description }}" />
    <!-- Open Graph Facebook, LinkeInd -->
    <meta name="og:title" content="{{ $eval->title }}"/>
    <meta name="image" property="og:image" content="{{ $eval->profiles->first()->photo_path }}" />
    <meta name="description" property="og:description" content="{{ $eval->description }}" />
    <meta name="keywords" content="">
    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@t-mido">
    <meta name="twitter:title" content="{{ $eval->title }}">
    <meta name="twitter:description" content="{{ $eval->description }}">
    <meta name="twitter:creator" content="@t-mido ">
    <meta name="twitter:image:src" content="{{ $eval->profiles->first()->photo_path }}">

    @else
    <title>T-Mido | T-Mido </title>

    @endif



    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.10/semantic.min.css">
    <link rel="stylesheet" href="{{url('/css/image-picker.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/css/tether.min.css">
    <link rel="stylesheet" href="/css/cropper.css">
    <link rel="stylesheet" href="/css/main.css">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" ></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.10/semantic.min.js" ></script>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <script src="/js/jquery.countdown.min.js"></script>
    <link href="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.css" rel="stylesheet" type="text/css" />
    <script src="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.js"></script>
    <script type="text/javascript" src="{{url('/js/image-picker.min.js')}}" ></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
    <script src="https://fengyuanchen.github.io/js/common.js"></script>
    <script src="/js/cropper.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js" ></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js" ></script>
    <script src="https://codepen.io/anon/pen/aWapBE.js"></script>
    <link href="/assets/lity/lity.min.css" rel="stylesheet">
    <script src="/assets/lity/lity.min.js"></script>

    <link href="/assets/floatingsocial/jquery.floating-social-share.css" rel="stylesheet">
    <script src="/assets/floatingsocial/jquery.floating-social-share.js"></script>

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <div class="ui large fixed top inverted huge menu">
          <div class="ui container" style="color:white;">
              <a href="/" class="">
                <img  class="logo_tmido" src="{{url('/img/logo_tmido_blanco.png')}}">
              </a>
            <div class="right menu" style="margin-right:25px;">

              @if (Auth::guest())
                <div style="line-height: 58px;">
                    <a href="{{ url('/register') }}" class="ui mini green button">Registro</a>
                </div>&nbsp;&nbsp;
                <div style="line-height: 58px;">
                    <a href="{{ route('login') }}" class="ui mini primary button">Login</a>
                </div>
              @else

                <a href="{{ url('/user_account') }}" class="tooltip" style="line-height: 58px;" data-content="{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}" >
                    <i class="user icon"></i>
                </a>
                <div style="line-height: 58px;">
                  <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"
                  class="ui red button mini" data-content="Cerrar Sesion!">Salir</a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                </div>

              @endif

            </div>
          </div>
        </div>

        @if (!Auth::guest() && Auth::user()->role!="user")
        <br>
        <br>
        <br>
        <div class="ui container">
            <div class="ui grid stackable">
                <div class="ui two column grid">

                @if (Auth::user()->role=="superadmin")
                   @include('layouts.menu_admin')
                @endif

                @if (Auth::user()->role=="company")
                   @include('layouts.menu_company')
                @endif
        @endif

        @include('layouts.functionsjs')

        @yield('content')
                </div>
            </div>
        </div>
    </div>

</body>
</html>
