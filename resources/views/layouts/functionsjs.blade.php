<script type="text/javascript">

// Carga Inicial
$(document).ready(function() {

    $('select.dropdown').dropdown();
    $('.checkbox').checkbox();
    $('.tooltip').popup();

    $('#date').calendar({
         type: 'datetime',
         ampm: false,
          monthFirst: false,
          formatter: {
            date: function (date, settings) {
              if (!date) return '';
              var day = date.getDate();
              var month = date.getMonth() + 1;
              var year = date.getFullYear();
              return day + '/' +month + '/' + year  ;
          }},
          text: {
               days: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
               months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
               monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
               today: 'Hoy',
               now: 'Ahora'
           }
    });

    $('.message .close').on('click', function() {
            $(this).closest('.message').transition('fade');
    });

    $('.info-modal-link').click(function() {
        $('.ui.modal').modal('show');
    });

    $('.item_rating').rating({
        maxRating: 5,
        onRate: function (rating) {
          var id = this.id;
          var id2 = id.split("_");
          $('#score_'+id2[1]).text(rating);
        }
    });

     $('#prom_rating').rating('disable');
     $('.pop').popup();
     $('.special.cards .image').dimmer({on: 'hover'});
     $('.popup-card').popup({
        inline: true,
        on: 'click',
        position: 'top center'
       });



});

/////////////////////////////////////////////
/////////////////////////////////////////////
//Funciones Generales Evaluacion y Votacion.

function selectCard(id)
{
    $('.cards .card').each(function(){
         $('#'+this.id).css('border','');
         $('#'+this.id).css('border-radius','');
    });
    $('#'+id).css('border','3px solid #21BA45');
    $('#'+id).css('border-radius','10px');
    $('#selected_profile_id').val(id);
}


function saveScoreItems()
{
    var scores = [];
    $(".items_factors").each(function(){
        var letter = $('#letter_'+this.id).html();
        var score = $('#score_'+this.id).html();
        var profile_id = $('#profile_id').html();
        var id = this.id;
        scores.push({letter: letter, score: score, id: id, profile : profile_id});
    });
    $('#scores_ev').val(JSON.stringify(scores));
    $('#save_score_form').submit();
}

function initCountdown(end_date)
{
    $('#countdown').countdown(end_date, function(event) {
           var totalDays = event.offset.totalDays;
           var $this = $(this).html(event.strftime(''
             + '<a class="ui label small red">D'
             + '<div class="detail">'+totalDays+'</div></a>'
             + '<a class="ui label small red " >H'
             + '<div class="detail">%H</div></a>'
             + '<a class="ui label small red" >M'
             + '<div class="detail">%M</div></a>'
             + '<a class="ui label small red ">S'
             + '<div class="detail">%S</div></a>'));

    });
}


function selectLike(id_factor,option)
{
    $('#score_'+id_factor+'').html(option);
	if(option=="like"){
		$("#factor_"+id_factor+"_like").css("color","#21BA45");
		$("#factor_"+id_factor+"_dlike").css("color","grey");
	}
	if(option=="dlike"){
		$("#factor_"+id_factor+"_like").css("color","grey");
		$("#factor_"+id_factor+"_dlike").css("color","#DB2828");
	}
}

function selectYesNot(id_factor,option)
{
    $('#score_'+id_factor+'').html(option);
	if(option=="yes"){
		$("#factor_"+id_factor+"_yes").css("color","#21BA45");
		$("#factor_"+id_factor+"_no").css("color","grey");
	}
	if(option=="no"){
		$("#factor_"+id_factor+"_yes").css("color","grey");
		$("#factor_"+id_factor+"_no").css("color","#DB2828");
	}
}
</script>
