<div class="four wide column">
    <div class="ui secondary vertical pointing menu">
      <a href="/statistics_admin" class="{{ $page == 'statistics_admin' ?  'item active' : 'item'}}">
         Estadisticas & Alertas
      </a>
      <a  href="/companies_admin" class="{{ $page == 'companies_admin' ?  'item active' : 'item'}}">
        Empresas
      </a>
      <a href="/accounts_admin" class="{{ $page == 'accounts_admin' ?  'item active' : 'item'}}">
        Planes
      </a>
      <a href="/payments_admin" class="{{ $page == 'payments_admin' ?  'item active' : 'item'}}">
        Pagos
      </a>
      <a href="/contacts_admin" class="{{ $page == 'contacts_admin' ?  'item active' : 'item'}}">
        Contactos
      </a>
    </div>
</div>
