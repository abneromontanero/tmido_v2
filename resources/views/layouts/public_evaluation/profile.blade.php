<div class="five wide column">
    <h4>
        {{ $single->title}}
    </h4>
    <div class="ui wide column">
        <!--<a class=" fb-xfbml-parse-ignore ui  mini circular facebook icon button" target="_blank" href="http://facebook.com/sharer.php?u=http://{{ $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] }}" title="Comparte en Facebook">
            <i class="facebook icon"></i>
        </a>

        <a class="ui mini circular twitter icon button " target="_blank" href="http://twitter.com/home?status=http://{{ $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] }}" title="Comparte en Twitter">
            <i class="twitter icon"></i>
        </a>

        <a class="ui  mini circular google plus icon button " target="_blank" href="https://plus.google.com/share?url=http://{{ $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] }}" title="Comparte en Google+">
            <i class="google plus icon link"></i>
        </a>-->

        <a class="ui floated right mini circular icon green button pop " href="#evaluation" data-content="ir a Evaluacion" data-variation="inverted" ><i class="checkmark box  icon"></i></a>
        <a class="ui floated right mini circular icon green button pop " href="#results" data-content="ir a Resultados" data-variation="inverted" ><i class="pie chart icon"></i></a>
    </div>
    <div class="ui card">
        <div class="image">
            <img src="{{ $profile->photo}} ">
        </div>
        <div class="content">
            <div class="header" style="font-size:14px;">{{ $profile->title }}</div>
            <div class="description" style="font-size:12px;">
                {{ $profile->description }}
            </div>
        </div>
        <div class="extra content">
            <a class="ui label mini"  href="{{ $profile->url_video }}" data-lity>
                <i class="record icon"></i> Video
            </a>
            <a>
                <a class="ui label mini pop" href="{{ $profile->url_document }}" data-lity>
                    <i class="fa fa-info-circle" ></i>  Informacion
                </a>
            </div>
        </div>
    </div>
