<div class="ui two column stackable grid">
    <div class="column">
        <a class="ui small  blue label pop" data-content="Visitas" data-variation="inverted">
            <i class="user icon"></i>
            {{ $single->visit != null ? $single->visit : 0 }} <br>
        </a>
        <a class="ui small green label pop" data-content="Evaluaciones Enviadas" data-variation="inverted">
            <i class="checkmark box  icon"></i>
            {{ $user_eval_send != null ? $user_eval_send : 0}} <br>
        </a>
    </div>
    <div class="column">
        <div class="ui grid">
            <div class="fourteen wide column" style="text-align: right;padding-right: 0;">
                <span class="container_countdown">
                    <strong><small>Cierra en: </small></strong>
                    <span id="countdown">
                    </span>
                </span>
            </div>
        </div>
    </div>
</div>
