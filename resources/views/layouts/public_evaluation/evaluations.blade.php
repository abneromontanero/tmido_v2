


<div class="fiveteen wide column">
@if($single->mode =="multiple")
<label class="ui blue label">Otros Competidores/Participantes</label>
<div class="ui segment">

    <div class="ui six special cards ">
        @forelse( $single->profiles as $profilex)
        <a class="card pop" href="/evaluation/{{ $single->id }}/profile/{{ $profilex->id }}" data-content="{{ $profilex->title }}" data-variation="inverted" >
            <div class="blurring dimmable image">
                <div class="ui dimmer">
                    <div class="content">
                        <div class="center">
                            <br>
                            <small>Evalua!</small>
                        </div>
                    </div>
                </div>
                <img src="{{$profilex->photo}}">
            </div>
        </a>
        @empty
        <span></span>
        @endforelse
    </div>
</div>

@endif

    @if($single->type == 'star_value')
        <small color="red">Valora presionando sobre las estrellas <div class="ui star rating"><i class="icon active"></i></div></small>
    @elseif($single->type == 'like_dlike')
        <small color="red">Evalua presionando los botones  Me Gusta / No Me Gusta a continuacion: </small>
    @elseif($single->type == 'yes_no')
        <small color="red">Evalua presionando Si / No </small>
    @endif
    <div class="ui divider"></div>
    @forelse($single->items as $item)
    <div class="ui grid items_factors" id="{{ $item->id }}">
        <span id="profile_id" style="display:none">{{ $profile->id }}</span>
        <div class=" one wide column">
            <span class="ui blue circular label" id="letter_{{ $item->id }}">{{ $letter_list[$loop->index] }}</span>
        </div>
        <div class="six wide column">
            <small>{{ $item->description}}</small>
        </div>
        @if($single->type == 'star_value')
            <div class="five wide column">
                <div class="ui star rating item_rating" id="rating_{{ $item->id }}" data-rating="{{ round($item->responses->where('profile_id',$profile->id)->avg('value'),0)}}" data-max-rating="5"></div>
                <small id="score_{{ $item->id }}">0</small>

            </div>
            <div class="two wide column center aligned">
                <div class="ui label small basic">
                    <small>PROM. </small>  {{ round($item->responses->where('profile_id',$profile->id)->avg('value'),1)}}
                </div>
            </div>

            @if($single->mode = 'multiple')
            <div class="one wide column">
                <a class="ui mini icon blue button pop" data-variation="inverted" href="/chart_item/{{ $item->id }}/profile/{{ $profile->id }}" data-content="Estadistica" data-lity><i class="pie chart icon"></i></a>
            </div>
            @endif




        @elseif($single->type == 'like_dlike')
            <div class="five wide column">
                <div class="ui buttons">
                    <button  class=" ui left attached icon button pop" data-variation="inverted" onclick="selectLike({{ $item->id }},'like')" data-content="Me Gusta">
                        &nbsp;&nbsp;<i class="fa fa-thumbs-up" id="factor_{{ $item->id }}_like" style="color:grey;"></i>&nbsp;&nbsp;<br>
                        <small style="font-size:9px;"> {{ $item->responses->where('value','like')->where('profile_id',$profile->id)->count() }} </small>
                    </button>

                    <button  class="right attached ui icon button pop" data-variation="inverted" onclick="selectLike({{ $item->id }},'dlike')" data-content="No Me Gusta">
                        &nbsp;&nbsp;<i class="fa fa-thumbs-down" id="factor_{{ $item->id }}_dlike" style="color:grey;"></i>&nbsp;&nbsp;<br>
                        <small  style="font-size:9px;"> {{ $item->responses->where('value','dlike')->where('profile_id',$profile->id)->count() }} </small>
                    </button>
                </div>

                <small style="display:none;" id="score_{{ $item->id }}"></small>
            </div>

        @elseif($single->type == 'yes_no')
            <div class="five wide column">
                <div class="ui buttons">
                    <button class="ui left attached icon button pop" data-variation="inverted" onclick="selectYesNot({{ $item->id }},'yes')" data-content="Si">
                        &nbsp;&nbsp; <span id="factor_{{ $item->id }}_yes" style="color:grey;">SI</span> &nbsp;&nbsp;<br>
                        <small  style="font-size:9px;"> {{ $item->responses->where('value','yes')->where('profile_id',$profile->id)->count() }} </small>
                    </button>
                    <button class="right attached ui icon button pop" data-variation="inverted" onclick="selectYesNot({{ $item->id }},'no')" data-content="No">
                        &nbsp;&nbsp; <span id="factor_{{ $item->id }}_no" style="color:grey;">NO</span> &nbsp;&nbsp;<br>
                        <small  style="font-size:9px;"> {{ $item->responses->where('value','no')->where('profile_id',$profile->id)->count() }} </small>
                    </button>
                </div>

            </div>
            <small style="display:none;" id="score_{{ $item->id }}"></small>

        @endif
    </div>
    @empty
    @endforelse
    <div class="ui divider"></div>
    @if($single->type == 'star_value')
    <div class="ui grid three column">
        <div class="column" style="text-align: center;font-weight:bold; ">
            PROM. TOTAL  <br>
            <div class="ui star rating huge"  id="prom_rating" data-rating="{{ round($total_avg) }}" data-max-rating="5"></div>
        </div>
        <div class="column">
            <div class="ui label blue big">{{ round($total_avg,1) }}</div>
        </div>
        <div class="column" style="">
            @if( $single->end_date > Carbon\Carbon::now() )
            <button type="button" onclick="saveScoreItems()" class="ui fluid circle button green"  >Enviar Evaluacion!</button>
            @else
            <span class="ui label red large" > Evaluacion Cerrada! </span>
            @endif
            <br>
        </div>
    </div>
    @else
    <div class="ui grid two column">
        <div class="column">
        </div>
        <div class="column" style="">
            @if( $single->end_date > Carbon\Carbon::now() )
            <button type="button" onclick="saveScoreItems()" class="ui fluid circle button green"  >Enviar Evaluacion!</button>
            @else
            <span class="ui label red large" > Evaluacion Cerrada! </span>
            @endif
            <br>
        </div>
    </div>
    @endif
</div>
