@if(session()->has('nologin'))
<div class="ui negative message">
    <i class="close icon"></i>
    <div class="header">
        Lo sentimos!
    </div>
    <p>{{ session()->get('nologin') }} <a href="/login?url={{ URL::current() }}"> Entrar </a>
    </p>
</div>
@endif
@if(session()->has('okeval'))
<div class="ui negative message">
    <i class="close icon"></i>
    <div class="header"> Lo sentimos! </div>
    <p>{{ session()->get('okeval')}} </p>
</div>
@endif
@if(session()->has('noactive'))
<div class="ui negative message">
    <i class="close icon"></i>
    <div class="header"> Lo sentimos! </div>
    <p>{{ session()->get('noactive')}} </p>
</div>
@endif
