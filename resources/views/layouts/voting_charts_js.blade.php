<script type="text/javascript">
var ctx = document.getElementById("barChart");

var myData = [
    @forelse($profiles as $profile)
    {{ $profile->votes }} ,
    @empty
    @endforelse
    ];
var myLabels = [
    @forelse($profiles as $profile)
    '{{ $profile->title }}',
    @empty
    @endforelse
];

var myChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
    labels: myLabels,
    datasets: [{
      label: '# of Votes',
      data: myData,
      backgroundColor: palette('tol', myData.length).map(function(hex) {
        return '#' + hex;
      })
    }]
  },
  options: {
      responsive: true,
      legend: {
          position: 'bottom',
      },
      title: {
          display: true,
          text: 'Resultados Votacion'
      },
      animation: {
          animateScale: true,
          animateRotate: true
      },
      tooltips: {
          callbacks: {
              label: function(tooltipItem, data) {
                     var dataset = data.datasets[tooltipItem.datasetIndex];
                     var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                         return previousValue + currentValue;
                     });
                     var currentValue = dataset.data[tooltipItem.index];
                     var precentage = Math.floor(((currentValue/total) * 100)+0.5);
                     return precentage + "%   "+data.labels[tooltipItem.index];
                 }
             }
         }
     }
});
</script>
