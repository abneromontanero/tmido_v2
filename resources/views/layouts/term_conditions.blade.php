<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Terminos y Condiciones</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.10/semantic.min.css">
</head>
<body style="background-color:white;padding:30px;">
    <h2 style="text-align: center;"><strong>T&eacute;rminos y Condiciones generales de uso y Pol&iacute;tica de Privacidad</strong></h2>
    <p>&ldquo;<strong>T-MIDO &reg;</strong>&rdquo; (L&eacute;ase Te mido), consiste en una herramienta digital, una plataforma web y app para dispositivos m&oacute;viles asociada, seg&uacute;n sea la modalidad, que tiene como prop&oacute;sito, medir y rankear la valoraci&oacute;n, evaluaci&oacute;n y preferencias de sus usuarios, respecto de sus percepciones ante diversas tem&aacute;ticas de inter&eacute;s, bajo el lema<strong>:&nbsp;&nbsp;&nbsp; &nbsp;&ldquo;T-MIDO&reg;, mide lo que a la gente le interesa&rdquo;.</strong></p>
    <p><strong>T-MIDO&reg; </strong>permite a sus usuarios registrados o no registrados seg&uacute;n las condiciones particulares de cada publicaci&oacute;n, sea privada o p&uacute;blica, a evaluar y valorar en distintos formatos, compartir con sus contactos de redes sociales, evaluar por diferentes conceptos, todo 100% on line, de forma transparente e informada 24x7, en los diferentes temas, desaf&iacute;os y publicaciones propuestos.</p>
    <p>Los T&eacute;rminos, Condiciones y Pol&iacute;tica de Seguridad, ac&aacute; detallados, describen las condiciones generales de los servicios que rigen nuestra relaci&oacute;n con los usuarios, clientes, proveedores, medios de comunicaci&oacute;n, alianzas con medios y empresas nacionales y/o internacionales y con todas aquellas personas naturales y jur&iacute;dicas de car&aacute;cter p&uacute;blico o privado, que interact&uacute;an a trav&eacute;s de nuestro sitio web <a href="http://www.T-MIDO.cl"><strong>www.T-MIDO.cl</strong></a> y/o sitios y aplicaciones m&oacute;viles asociadas.</p>
    <p><strong>T-MIDO</strong><strong>&reg;</strong> no representa a partidos pol&iacute;ticos, candidatos o tendencias pol&iacute;ticas en particular. Deseamos abrir las puertas a todos los usuarios para que se expresen, libre, transparente y democr&aacute;ticamente, en forma, activa, positiva y tambi&eacute;n respetuosa de otras opiniones, comentarios o ideas aportadas. Por lo anterior, &nbsp;&nbsp;&nbsp;<strong>T-MIDO&reg; </strong>no se hace responsable de las opiniones o comentarios que los usuarios emitan.</p>
    <p><strong>T-MIDO&reg;</strong> es una herramienta digital de uso y utilidad transversal a personas, empresas y organizaciones p&uacute;blicas o privadas. La creatividad de nuestros usuarios y clientes ser&aacute; siempre bienvenida, por lo que los invitamos a enviarnos sus sugerencias, observaciones e ideas para que nos permitan mejorar e innovar constantemente en beneficio de todos. <a href="mailto:contacto@t-mido.cl">contacto@t-mido.cl</a>.</p>
    <p>&nbsp;</p>
    <p><strong>POL&Iacute;TICA DE PRIVACIDAD</strong></p>
    <ol>
        <li><strong> General</strong></li>
    </ol>
    <p><strong>T-MIDO &reg;</strong> respeta la privacidad de todas las personas que visitan nuestro sitio web y/o acceden a las aplicaciones para dispositivos m&oacute;viles asociadas. Esta Pol&iacute;tica de Privacidad refleja la informaci&oacute;n que nuestro sitio web y aplicaciones recogen y c&oacute;mo utilizaremos dicha informaci&oacute;n. Al respecto, <strong>T-MIDO &reg; </strong>desea proporcionar a sus usuarios el m&aacute;ximo control a su alcance sobre la protecci&oacute;n de la informaci&oacute;n que les identifica personalmente. De esta manera, cada usuario puede:</p>
    <p>(a) Acceder a su informaci&oacute;n personal, a trav&eacute;s de su perfil de usuario;</p>
    <p>(b) Retirar su Informaci&oacute;n personal de nuestra base de datos, borrando su cuenta de usuario. Este acto no afectar&aacute; los comentarios que haya realizado con anterioridad ni aquellos datos que por prop&oacute;sitos estad&iacute;sticos hayan sido conservados para mantener la transparencia y fidelidad de las opiniones vertidas en nuestra plataforma; y,</p>
    <p>(c) Corregir la Informaci&oacute;n Personal que haya manifestado ser err&oacute;nea.</p>
    <p>&nbsp;</p>
    <ol start="2">
        <li><strong> Informaci&oacute;n personal</strong></li>
    </ol>
    <p>Cuando una persona visite nuestro sitio web o acceda a las aplicaciones para dispositivos m&oacute;viles asociadas, no recogeremos ninguna Informaci&oacute;n personal (como su nombre, direcci&oacute;n, n&uacute;mero de tel&eacute;fono, n&uacute;mero de identificaci&oacute;n, o direcci&oacute;n de correo electr&oacute;nico), a menos que dicha visita se registre como usuario y que estos datos sean proporcionados voluntariamente a nosotros a fin de participar de las votaciones y evaluaciones. Al utilizar nuestro sitio web o aplicaciones para dispositivos m&oacute;viles, cada usuario se compromete a aceptar los t&eacute;rminos y condiciones de nuestra Pol&iacute;tica de Privacidad.</p>
    <p>&nbsp;</p>
    <ol start="3">
        <li><strong> Tratamiento o de los datos de car&aacute;cter personal</strong></li>
    </ol>
    <p><strong>T-MIDO &reg; </strong>proporciona a los usuarios los recursos t&eacute;cnicos adecuados para que, con car&aacute;cter previo, puedan acceder a esta Pol&iacute;tica de Privacidad y puedan dar su consentimiento a fin de que <strong>T-MIDO &reg; </strong>proceda al tratamiento automatizado de sus datos personales. En cuanto a los formularios electr&oacute;nicos de registro y recogida de datos del sitio web o aplicaciones para dispositivos m&oacute;viles asociados, salvo en los campos en que se indique lo contrario, las respuestas a las preguntas sobre datos personales son voluntarias, sin que la falta de contestaci&oacute;n implique una disminuci&oacute;n en la calidad o cantidad de los servicios correspondientes, a menos que se indique otra cosa, en atenci&oacute;n a la validaci&oacute;n, credibilidad y confiabilidad en las mediciones efectuadas.</p>
    <p>Cuando usted nos suministre voluntariamente datos de car&aacute;cter personal, nos est&aacute; autorizando para utilizar dicha Informaci&oacute;n personal de acuerdo con los t&eacute;rminos y condiciones de nuestra Pol&iacute;tica de Privacidad.</p>
    <p>El usuario garantiza que los datos personales facilitados a <strong>T-MIDO &reg; </strong>son veraces y se hace responsable de comunicar a &eacute;sta cualquier modificaci&oacute;n en los mismos.</p>
    <p><strong>T-MIDO &reg; </strong>puede guardar y procesar sus datos personales para la generaci&oacute;n de gr&aacute;ficos, estad&iacute;sticas e informes y, tambi&eacute;n para entender mejor sus opiniones, necesidades y el modo en el que dichas opiniones contribuyen a mejorar los an&aacute;lisis de los temas sometidos a medici&oacute;n. <strong>T-MIDO &reg; </strong>puede utilizar su informaci&oacute;n personal para tomar contacto con usted, por ejemplo, para premiar su participaci&oacute;n, solicitar su colaboraci&oacute;n en una mejor experiencia usuaria, personalizar la informaci&oacute;n de los productos o servicios que ofrecemos, enviarles materiales de marketing o promoci&oacute;n o para poder responder a sus comentarios o solicitudes de informaci&oacute;n. En todo caso, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>T-MIDO &reg; </strong>no vende, arrienda ni cede a ning&uacute;n t&iacute;tulo oneroso informaci&oacute;n personal.</p>
    <p>Cualquier cambio en esta Pol&iacute;tica de Privacidad ser&aacute; publicado y actualizado en esta p&aacute;gina web y aplicaciones para dispositivos m&oacute;viles si corresponde. Esto le permite conocer en cualquier momento qu&eacute; informaci&oacute;n estamos guardando y c&oacute;mo recolectamos y usamos dicha informaci&oacute;n.</p>
    <p>&nbsp;</p>
    <ol start="4">
        <li><strong> Seguridad</strong></li>
    </ol>
    <p><strong>T-MIDO &reg; </strong>se compromete a tratar su Informaci&oacute;n personal con privacidad, confidencialidad y seguridad y a proteger sus datos personales mediante todos los medios t&eacute;cnicos a su alcance, ante de la p&eacute;rdida, mal uso, acceso no autorizado, alteraci&oacute;n y destrucci&oacute;n.</p>
    <p>Empresas externas a <strong>T-MIDO &reg; </strong>que tengan acceso a sus datos personales en relaci&oacute;n con los servicios prestados a <strong>T-MIDO &reg; </strong>estar&aacute;n obligados a mantener la informaci&oacute;n d forma confidencial y no tendr&aacute;n permiso para utilizar esta informaci&oacute;n para cualquier otra finalidad que no sea en el &aacute;mbito de los servicios y actividades que est&aacute;n realizando para <strong>T-MIDO&reg;</strong>. Al proporcionarnos su informaci&oacute;n, usted acepta que las referidas empresas externas a <strong>T-MIDO &reg;</strong>puedan tener acceso a sus datos personales en raz&oacute;n de los servicios o relaci&oacute;n comercial que estas empresas tienen con <strong>T-MIDO&reg;</strong>. En algunos casos, ser&aacute; necesario que transfiramos sus consultas a compa&ntilde;&iacute;as afiliadas a <strong>T-MIDO&reg;</strong>. Tambi&eacute;n en estos casos sus datos ser&aacute;n tratados de manera confidencial.</p>
    <p><strong>T-MIDO &reg; </strong>podr&iacute;a llegar a revelar datos personales si fuese debida y formalmente &nbsp;requerido, s&oacute;lo por ley, tribunal o autoridad competente.</p>
    <p>&nbsp;</p>
    <ol start="5">
        <li><strong> Menores</strong></li>
    </ol>
    <p><strong>T-MIDO &reg; </strong>pide a los padres o tutores que informen a los menores de edad acerca del uso responsable y seguro de sus datos de car&aacute;cter personal cuando participen en actividades on-line.</p>
    <p><strong>T-MIDO &reg; </strong>no tiene intenci&oacute;n de recoger Informaci&oacute;n personal de menores de 18 a&ntilde;os. Cuando sea necesario, <strong>T-MIDO &reg; </strong>dar&aacute; instrucciones espec&iacute;ficas a los menores para que no proporcionen datos de car&aacute;cter personal en nuestros anuncios o sitios web.</p>
    <p>Si el menor nos ha proporcionado datos de car&aacute;cter personal, el padre o tutor del menor deber&aacute; ponerse en contacto con nosotros a trav&eacute;s de nuestros correos electr&oacute;nicos indicados en esta Pol&iacute;tica de Privacidad si desea que cancelemos dicha informaci&oacute;n de nuestros registros. Dedicaremos todos los esfuerzos razonables a nuestro alcance a fin de cancelar la informaci&oacute;n del menor de nuestras bases de datos.</p>
    <p>&nbsp;</p>
    <ol start="6">
        <li><strong> Informaci&oacute;n no personal recogida autom&aacute;ticamente</strong></li>
    </ol>
    <p>Nuestro servicio web recoge autom&aacute;ticamente determinada informaci&oacute;n no personal sobre el uso de nuestro sitio web, que se almacena en nuestros servidores para fines exclusivamente internos, como pueden ser facilitar su visita a nuestro sitio web, mejorar su experiencia on-line o para finalidades estad&iacute;sticas de acceso.</p>
    <p>Ejemplos de este tipo de informaci&oacute;n no personal incluyen el nombre de su proveedor de servicios de Internet, el tipo de navegador de Internet o el sistema operativo utilizado por usted y el nombre de dominio del sitio web desde el cual ha llegado a nuestro sitio o anuncio.</p>
    <p>En caso de cesi&oacute;n de datos a un proveedor de servicios externo, se aplicar&aacute;n las medidas t&eacute;cnicas y organizativas a fin de garantizar una cesi&oacute;n conforme a las disposiciones en materia de protecci&oacute;n de datos.</p>
    <p>Cuando usted ve una de nuestras p&aacute;ginas web, podemos almacenar cierta informaci&oacute;n en su computador en forma de "cookie" o similar que nos ayudar&aacute; en diversas formas, como por ejemplo, permitirnos adecuar una p&aacute;gina web o anuncio a sus intereses y preferencia. En la mayor&iacute;a de navegadores de Internet usted puede eliminar las "cookies" del disco duro de su computador, bloquear todas las "cookies" o recibir un aviso antes de que se instale una "cookie". Por favor, consulte las instrucciones de su navegador o la pantalla de ayuda para saber m&aacute;s sobre el funcionamiento de estas funciones.</p>
    <p>&nbsp;</p>
    <ol start="7">
        <li><strong> Pol&iacute;tica de Cookies</strong></li>
    </ol>
    <p>Nuestros sitios web podr&aacute;n incorporar cookies de origen y de terceros con el objetivo de mejorar los servicios que le ofrecemos. Puede permitir o no el uso de cookies en el sitio, siempre habiendo la posibilidad de configurar el navegador para ser avisado de la recepci&oacute;n de cookies e impedir su instalaci&oacute;n en el equipo. No obstante, la desactivaci&oacute;n de las cookies puede afectar al correcto funcionamiento de determinadas secciones del sitio web.</p>
    <p>Utilizamos cookies para que pueda sacar m&aacute;s provecho de nuestro sitio web. Las cookies son peque&ntilde;os bloques de informaci&oacute;n que su navegador almacena temporalmente en el disco duro del dispositivo y son necesarias para poder navegar por nuestra web. Las cookies incluyen a menudo un &uacute;nico identificador: un n&uacute;mero an&oacute;nimo que se genera aleatoriamente y se almacena en su dispositivo. Algunas son temporales y expiran cuando el usuario finaliza la navegaci&oacute;n en la p&aacute;gina web mientras que otras permanecen por un tiempo en el dispositivo.</p>
    <p>&nbsp;</p>
    <ol start="8">
        <li><strong> El uso de complementos de redes sociales</strong></li>
    </ol>
    <p>Los complementos (&ldquo;plug-ins&rdquo;) de las redes sociales Facebook y Twitter pueden ser incorporados en nuestras p&aacute;ginas web. Los servicios asociados provienen respectivamente de las compa&ntilde;&iacute;as Facebook Inc. y Twitter Inc. (o &ldquo;proveedores&rdquo;).</p>
    <p>La red social de Facebook es operada por Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, USA ("Facebook"). Para conocer m&aacute;s detalles de los complementos de Facebook y su apariencia visite: https://developers.facebook.com/docs/plugins</p>
    <p>Twitter es operado por Twitter Inc., 1355 Market St, Suite 900, San Francisco, CA 94103, USA. Para conocer m&aacute;s detalles de los complementos de Twitter y su apariencia visite: https://twitter.com/about/resources/buttons</p>
    <p>Para mejorar la protecci&oacute;n de sus datos, cuando visite nuestras p&aacute;ginas web o aplicaciones para dispositivos m&oacute;viles, estos complementos se implementan como botones sociales. Esta forma de integraci&oacute;n asegura que, al acceder a una de las p&aacute;ginas que contienen estos complementos, el usuario no se conecta de forma autom&aacute;tica a los servidores de los proveedores. S&oacute;lo con la activaci&oacute;n de los complementos el navegador crear&aacute; un enlace directo a los servidores de los proveedores. El contenido de varios complementos se transmite entonces desde el respectivo proveedor directamente al navegador del usuario y posteriormente aparecer&aacute; en la pantalla.</p>
    <p>Los complementos informan al proveedor de las p&aacute;ginas que ha visitado el usuario. Cuando se navega por nuestra web conectado con el nombre de usuario de Facebook o Twitter, el proveedor puede visualizar los intereses del usuario, es decir la informaci&oacute;n que est&aacute; visitando.&nbsp; Haciendo uso de estas opciones (por ejemplo, hacer un &ldquo;me gusta&rdquo; o escribir un comentario), esta informaci&oacute;n tambi&eacute;n se transmitir&aacute; a su cuenta de usuario de Facebook y Twitter.</p>
    <p>Informaci&oacute;n adicional sobre la recogida y uso de estos datos que haga Facebook y Twitter as&iacute; como los derechos y posibilidades disponibles para proteger su privacidad en este contexto se puede encontrar en la informaci&oacute;n de protecci&oacute;n de datos de Facebook y Twitter:</p>
    <p>Protecci&oacute;n de datos/ privacidad de Facebook: <a href="http://www.facebook.com/policy.php">http://www.facebook.com/policy.php</a></p>
    <p>Protecci&oacute;n de datos/ privacidad de Twitter: <a href="https://twitter.com/privacy">https://twitter.com/privacy</a></p>
    <p>Para evitar que Facebook y Twitter pueda relacionar la visita a nuestras p&aacute;ginas con su cuenta de usuario debe desconectar su cuenta de usuario antes de visitar nuestras webs.</p>
    <p>&nbsp;</p>
    <ol start="9">
        <li><strong> C&oacute;digo de Conducta del Usuario</strong></li>
    </ol>
    <p>A fin de que nuestra plataforma pueda servir a los prop&oacute;sitos y objetivos generales expresados en este documento, mucho agradeceremos tu cooperaci&oacute;n en las siguientes acciones y conductas deseadas al interactuar como usuario de nuestro sitio web o aplicaciones para dispositivos m&oacute;viles asociadas, cuando as&iacute; corresponda:</p>
    <ul>
        <li>No intentar&aacute;s, ni cometer&aacute;s acciones tendientes a afectar el correcto funcionamiento de nuestro sitio web, ni a inhabilitarlo, sobrecargarlo o a alterar su aspecto visual de textos, im&aacute;genes o funciones operativas determinadas.</li>
    </ul>
    <p>&nbsp;</p>
    <ul>
        <li>En tu calidad de usuario, eres libre de interactuar en nuestro sitio, pero te har&aacute;s responsable de los comentarios y contenidos que publiques, como asimismo, de sus consecuencias derivadas.</li>
    </ul>
    <p>&nbsp;</p>
    <ul>
        <li>No subir&aacute;s virus ni c&oacute;digo malicioso de ning&uacute;n tipo.</li>
    </ul>
    <p>&nbsp;</p>
    <ul>
        <li>No difamar&aacute;s ni cometer&aacute;s acciones que puedan molestar, intimidar, discriminar o acosar a ning&uacute;n usuario o persona que aparezca en las publicaciones a evaluar.</li>
    </ul>
    <p>&nbsp;</p>
    <ul>
        <li>Ten presente que tus intervenciones y los contenidos que env&iacute;es, publiques o exhibas en nuestro sitio, ser&aacute;n vistos por todo tipo de usuarios que merecen el mismo respeto que t&uacute;, por lo que no publicar&aacute;s contenido y/o im&aacute;genes obscenas o que contenga lenguaje que incite al odio, a la violencia, sean racistas, pornogr&aacute;ficos o contravengan las leyes.</li>
    </ul>
    <p>&nbsp;</p>
    <ul>
        <li>No actuar&aacute;s de manera fraudulenta suplantando a otras personas ni acceder&aacute;s a una cuenta ajena perteneciente a otro usuario.</li>
    </ul>
    <p>&nbsp;</p>
    <ul>
        <li>No publicar&aacute;s contenidos comerciales no autorizados previamente (como spam).</li>
    </ul>
    <p>&nbsp;</p>
    <p>Infringir cualquiera de estas normas de conducta podr&iacute;a acarrear responsabilidades civiles y penales que afectar&aacute;n directamente a sus autores, c&oacute;mplices y encubridores.</p>
    <p><strong>T-MIDO &reg;</strong>&nbsp; no est&aacute; en condiciones, ni dispone de los medios para ejercer controles y evaluaciones sobre la autenticidad, la veracidad, la legalidad, calidad t&eacute;cnica, viabilidad, originalidad o validez cient&iacute;fica de las opiniones, comentarios e Ideas aportadas por los usuarios.&nbsp; Cada&nbsp;usuario se obliga a usar su libre facultad de aportar y publicar sus ideas, propuestas, comentarios y votaciones en el sitio,&nbsp; de forma responsable, respetuosa, correcta y l&iacute;cita.</p>
    <p>En particular, cada usuario tambi&eacute;n se compromete a abstenerse de infringir, eludir o manipular eventuales derechos de propiedad intelectual. Asimismo, los usuarios y participantes se obligan a custodiar y mantener la seguridad y secreto de sus contrase&ntilde;as utilizadas al registrarse y a asumir las consecuencias de los perjuicios sufridos por la falta de diligencia de la custodia de las mismas.</p>
    <p><strong>T-MIDO</strong>, constituye una marca de la empresa Innovafull S.p.A.</p>
    <p>Favor, agradeceremos su contacto para reclamos, sugerencias, ideas de negocios o, para recibir apoyo para una mejor experiencia de usuario al e-mail: <strong>contacto@t-mido.cl.</strong></p>

</body>
</html>
