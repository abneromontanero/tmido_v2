<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.10/semantic.min.css">

        <script src="https://codepen.io/anon/pen/aWapBE.js"></script>

        <style media="screen">
            html{

            }
            body{
                padding: 15px;

            }
            .content{

            }
        </style>
    </head>
    <body>
        <div class="content">
            <h5 class="ui header divided">
                <i class="pie chart icon"></i>  Resultados :: {{ $item->description }}
            </h5>
                <span class="ui label blue">{{ $responses->count() }} Usuarios Evaluaron este Factor.</span>
                    {{ $responses }}
                    <div class="column">
                        @if( $item->evaluation->type == "like_dlike")
                        <canvas class="ui segment" id="c2" height="150px" ></canvas>
                        <script type="text/javascript">
                            var myData = [ {{ $responses->where('value','like')->count() }} ,{{ $responses->where('value','dlike')->count() }}];
                            var myLabels = ["Me Gusta","No Me Gusta"];
                        </script>
                        @endif

                        @if( $item->evaluation->type == "yes_no")
                        <canvas class="ui segment" id="c2" height="150px" ></canvas>
                        <script type="text/javascript">
                            var myData = [ {{ $responses->where('value','yes')->count() }} ,{{ $responses->where('value','no')->count() }}];
                            var myLabels = ["SI","NO"];
                        </script>
                        @endif

                        @if( $item->evaluation->type == "star_value")
                        @endif
                    </div>

            </div>

            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" ></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js" ></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js" ></script>
            <script type="text/javascript">

            /**var ctx = document.getElementById("c1");
            var myChart = new Chart(ctx, {
              type: 'bar',
              data: {
                  labels: [
                    'Me Gusta','No Me Gusta','Neto'
                    ],
                  datasets: [
                      {
                          type: 'bar',
                          label: 'Suma de {{ $item->description }}',
                          backgroundColor: 'rgba(54, 162, 235,1)',
                          data: [ 3,4,1

                              ],
                      }
                  ]
              },
              options: {
                scales: {
                  yAxes: [{
                    ticks: {
                      beginAtZero: true
                    }
                  }]
                }
              }
          });**/


            var ctx2 = document.getElementById("c2");
            var myChart = new Chart(ctx2, {
            type: 'doughnut',
            data: {
            labels: myLabels,
            datasets: [{
              label: '# Total Factor',
              data: myData,
              backgroundColor: palette('tol', myData.length).map(function(hex) {
                return '#' + hex;
              })
            }]
            },
            options: {
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: '# Total Factor'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            },
            tooltips: {
                callbacks: {
                    label: function(tooltipItem, data) {
                           var dataset = data.datasets[tooltipItem.datasetIndex];
                           var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                               return previousValue + currentValue;
                           });
                           var currentValue = dataset.data[tooltipItem.index];
                           var precentage = Math.floor(((currentValue/total) * 100)+0.5);
                           return precentage + "%   "+data.labels[tooltipItem.index];
                       }
                   }
               }
            }
            });

            </script>
    </body>

</html>
