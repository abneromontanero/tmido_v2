

<script type="text/javascript">
var ctx = document.getElementById("chart1");
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
      labels: [
      @if($single->type == 'star_value')
        @forelse ($single->items as $item)
            "{{ $letter_list[$loop->index] }}",
        @empty
        @endforelse
      @elseif($single->type == 'like_dlike')
        'Me Gusta','No Me Gusta','Neto'
      @elseif($single->type == 'yes_no')
        'SI','NO','Neto'
      @endif
      ],
      datasets: [
          {
              type: 'bar',
               @if($single->type == 'star_value')
              label: 'Promedio de Factores',
              @else
              label: 'Total de Factores',
              @endif
              backgroundColor: 'rgba(54, 162, 235,1)',
              data: [
                  @if($single->type == 'star_value')
                    @foreach ($single->items as $item)
                      {{ round($item->responses->where('profile_id',$profile->id)->avg('value'), 1) }},
                    @endforeach
                  @elseif($single->type == 'like_dlike')
                      <?php
                        $mg_nmg = explode(",", $total_avg);
                        echo $mg_nmg[0].','.$mg_nmg[1].','.($mg_nmg[0]-$mg_nmg[1]);
                       ?>
                  @elseif($single->type == 'yes_no')
                      <?php
                        $yes_no = explode(",", $total_avg);
                        echo $yes_no[0].','.$yes_no[1].','.($yes_no[0]-$yes_no[1]);
                       ?>
                  @endif
              ],
          }
      ]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true
        }
      }]
    }
  }
});


@if($single->type != 'star_value')


var ctx2 = document.getElementById("chart2");

var myData = [{{ $total_avg }}];
var myLabels = [
@if($single->type == 'yes_no') "Si","No" @endif
@if($single->type == 'like_dlike') "Me Gusta","No Me Gusta" @endif
];

var myChart = new Chart(ctx2, {
type: 'doughnut',
data: {
labels: myLabels,
datasets: [{
  label: '# Totales',
  data: myData,
  backgroundColor: palette('tol', myData.length).map(function(hex) {
    return '#' + hex;
  })
}]
},
options: {
responsive: true,
legend: {
    position: 'top',
},
title: {
    display: true,
    text: 'Resultados Totales'
},
animation: {
    animateScale: true,
    animateRotate: true
},
tooltips: {
    callbacks: {
        label: function(tooltipItem, data) {
               var dataset = data.datasets[tooltipItem.datasetIndex];
               var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                   return previousValue + currentValue;
               });
               var currentValue = dataset.data[tooltipItem.index];
               var precentage = Math.floor(((currentValue/total) * 100)+0.5);
               return precentage + "%   "+data.labels[tooltipItem.index];
           }
       }
   }
}
});

@endif

</script>
