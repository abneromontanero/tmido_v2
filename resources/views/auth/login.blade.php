@extends('layouts.app')
@section('content')
<div class="ui middle aligned center aligned grid">
  <div class="column">
    <form class="ui large form" role="form" method="POST" action="{{ route('login') }}">
      {{ csrf_field() }}
      <div class="ui stacked" style="margin:10px;">
          <br>
          <br>
          <h5><div class="ui dividing header">Login</div></h5>

          @if(session()->has('account_active'))
          <div class="ui form success" style="text-align: left;">
              <div class="ui success message" >
                    <div class="header">Felicidades tu cuenta esta activa!</div>
                    <p>Para entrar debes logearte a continuacion...</p>
              </div>
          </div>
          @endif

          @if(session()->has('account_inactive'))
          <div class="ui form success" style="text-align: left;">
              <div class="ui danger message" >
                    <div class="header">Lo sentimos!</div>
                    <p>No existe una cuenta asociada al token, contacta al administrador!</p>
              </div>
          </div>
          @endif

        <div class="field">
          <div class="ui mini left icon input">
            <i class="mail icon"></i>
            <input name="email" placeholder="E-mail"  id="email" type="email" value="{{ old('email') }}" required autofocus>
          </div>
          @if ($errors->has('password'))
              <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
          @endif
        </div>
        <div class="field">
          <div class="ui mini  left icon input">
            <i class="lock icon"></i>
            <input id="password" type="password" name="password"  placeholder="Password">
          </div>
          @if ($errors->has('password'))
              <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
          @endif
        </div>
        <div class="field" >
            <a href="/password/reset">¿ Olvidaste tu contraseña ?</a>
        </div>
        <button type="submit" class="ui fluid blue submit button" >
            <i class="sign in icon"></i>&nbsp;&nbsp;Entrar
        </button>
      </div>

      <div class="ui error message"></div>

    </form>

  </div>
</div>

<style type="text/css">
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
@endsection
