@extends('layouts.app')
@section('content')

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <form class="ui large form" role="form" method="POST" action="{{ route('password.request') }}">
      {{ csrf_field() }}
      <input type="hidden" name="token" value="{{ $token }}">
      <div class="ui stacked" style="margin:10px;">
          <br>
          <h3><div class="ui dividing header">Cambiar Password</div></h3>
           <br>

           <div class="field {{ $errors->has('email') ? 'error' : '' }}">
               <label style="float:left;">Email</label>
               <div class="ui  input">
                   <div class="ui left icon input">
                     <i class="mail icon"></i>
                     <input name="email" placeholder="Ingrese E-mail"  id="email" type="email" value="{{ old('email') }}"  required>
                   </div>
               </div>
           </div>


           <div class="field {{ $errors->has('password') ? 'error' : '' }}">
               <label style="float:left;">Password</label>
               <div class="ui  input">
                   <div class="ui left icon input">
                     <i class="lock icon"></i>
                     <input name="password" id="password" type="password"  required>
                   </div>
               </div>
           </div>

           <div class="field {{ $errors->has('password_confirmation') ? 'error' : '' }}">
               <label style="float:left;">Confirma Password</label>
               <div class="ui  input">
                   <div class="ui left icon input">
                     <i class="lock icon"></i>
                     <input name="password_confirmation"  id="password-confirm" type="password"   required>
                   </div>
               </div>
           </div>

        <button type="submit" class="ui fluid green submit button" >
            Cambiar Password
        </button>
      </div>
      @if (session('status'))
          <div class="ui error message">
              {{ session('status') }}
          </div>
      @endif
    </form>

  </div>
</div>

<style type="text/css">
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>



@endsection
