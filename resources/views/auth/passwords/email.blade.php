@extends('layouts.app')
@section('content')

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <form class="ui large form" role="form" method="POST" action="{{ route('password.email') }}">
      {{ csrf_field() }}
      <div class="ui stacked" style="margin:10px;">
          <br>
          <h3><div class="ui dividing header">Envio Cambio Password</div></h3>
           <br>
           <div class="field {{ $errors->has('email') ? 'error' : '' }}">
               <label style="float:left;">Email</label>
               <div class="ui  input">
                   <div class="ui left icon input">
                     <i class="mail icon"></i>
                     <input name="email" placeholder="Ingrese E-mail"  id="email" type="email" value="{{ old('email') }}"  >
                   </div>
               </div>
           </div>
           @if ($errors->has('email'))
               <span>
                   <strong>{{ $errors->first('email') }}</strong>
               </span>
           @endif

        <button type="submit" class="ui fluid green submit button" >
            Enviar link Cambio Password
        </button>
      </div>
      @if (session('status'))
          <div class="ui error message">
              {{ session('status') }}
          </div>
      @endif
    </form>

  </div>
</div>

<style type="text/css">
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
  </style>
@endsection
