@extends('layouts.app')
@section('content')
<div class="ui client_ container">

    <div class="ui segment " style="background-color:#1B1C1D;color:white;">
        <div class="ui inverted secondary menu">
            <a href="/company/{{ $company->id}}" style="">
                @if($company->photo)
                <img class="logo_company ui rounded image" src="/img/companies/{{ $company->photo}}" style="max-height:55px;">
                @else
                <img class="logo_company ui rounded image" src="/img/company_default.png" style="max-height:55px;">
                @endif
            </a>
            <span style="font-size:13px;font-weight:bold;line-height: 60px;margin-left: 30px;">{{ $company->company_name}}</span>
        </div>
    </div>

    <div class="ui breadcrumb segment">
      <a href="/company/{{ $company->id}}">Inicio</a>
      <i class="right angle icon divider"></i>
      <div class="active section">Competencia</div>
    </div>

    <div class="ui two column stackable grid">
        <div class="ten wide column">
                <a name="profile" class="anchor"></a><br>
                @include('layouts.public_evaluation.messages')

                <div class="ui two column stackable grid" >
                @include('layouts.public_evaluation.profile')
                    <a name="evaluation" class="anchor"></a>
                    <div class="ui ten wide column grid" >
                        @include('layouts.public_evaluation.evaluations')
                    </div>
                </div>
                <div class="ui ten column">
                    <div class="ui divider"></div>
                        @include('layouts.public_evaluation.visit_countdown')
                    <div class="ui divider"></div>
                </div>
                <br>
                <div class="ui ten column">
                </div>
            </div>
            <a name="results" class="anchor"></a>
            @include('layouts.public_evaluation.results_charts')
        </div>
        @include('layouts.footer')
    </div>
    <div id="inline" style="background:#fff" class="lity-hide">
        Inline content
    </div>
    <form id="save_score_form" action="/save_score_items" method="post" style="display:none;">
        {{ csrf_field() }}
        <input id= "evaluation_id" type="hidden" name="evaluation_id" value="{{ $single->id }}">
        <input id= "scores_ev" type="hidden" name="scores" value="">
    </form>
    <script type="text/javascript">
    initCountdown('{{ $single->end_date }}');
    $("body").floatingSocialShare({
           place: "top-left", // alternatively content-left, content-right, top-right
           counter: true, // set to false for hiding the counters of buttons
           twitter_counter: false, // Twitter API does not provide counters without API key, register to https://opensharecount.com/
           buttons: [ // all of the currently available social buttons
             "mail", "facebook", "google-plus","pinterest","twitter", "whatsapp"
           ],
           title: document.title, // your title, default is current page's title
           url: window.location.href,  // your url, default is current page's url
           text: { // the title of tags
             'default': 'Compartir con ',
             'facebook': 'Compartir con facebook',
             'google-plus': 'Compartir con g+'
           },
           text_title_case: $('meta[name="title"]').attr("content"), // if set true, then will convert share texts to title case like Share With G+
           description: $('meta[name="description"]').attr("content"), // your description, default is current page's description
           media: $('meta[property="og:image"]').attr("content"), // pinterest media
           popup: true, // open links in popup
           popup_width: 400, // the sharer popup width, default is 400px
           popup_height: 300 // the sharer popup height, default is 300px
     });
    </script>
    @include('layouts.evaluations_charts_js')
    @endsection
