@extends('layouts.app')
@section('content')

<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
    </h2>
    <form class="ui large form" action="/user_register" role="form" method="POST" >
      {{ csrf_field() }}
      <div class="ui stacked" style="margin:10px;">
          <br><div class="ui dividing header">Registro</div></h5>

          <div class="field {{ $errors->has('first_name') ? 'error' : '' }}">
              <label style="float:left;">Nombre *</label>
              <div class="ui  input">
                  <div class="ui mini left icon input">
                    <i class="user icon"></i>
                    <input name="first_name" placeholder="Ingrese Nombre"  id="first_name" type="first_name" value="{{ old('first_name') }}"  >
                  </div>
              </div>
          </div>
        <div class="field {{ $errors->has('email') ? 'error' : '' }}">
            <label style="float:left;">Email *</label>
            <div class="ui  input">
                <div class="ui mini left icon input">
                  <i class="mail icon"></i>
                  <input name="email" placeholder="Ingrese E-mail"  id="email" type="email" value="{{ old('email') }}"  >
                </div>
            </div>
        </div>
        <div class="field {{ $errors->has('password') ? 'error' : '' }}">
            <label style="float:left;">Password *</label>
            <div class="ui  input">
                <div class="ui mini left icon input">
                  <i class="lock icon"></i>
                  <input name="password" placeholder="Ingrese Password"  id="password" type="password"  >
                </div>
            </div>
        </div>
        <div class="field">
            <label style="float:left;">Genero *</label>
            <div class="ui mini input">
                <select class="dropdown" name="gender" id="gender" required>
                    <option value="m">Masculino</option>
                    <option value="f">Femenino</option>
                </select>
            </div>
        </div>

        <div class="field">
            <label style="float:left;">Fecha de Nacimiento *</label>
            <div class="ui  mini input">
                <div class="ui " id="">
                  <div class="ui  input left icon">
                    <i class="calendar icon"></i>
                    <input type="text" class="" name="birthdate" placeholder="Dia/Mes/Año" id="birthdate" maxlength="10"  required>
                  </div>
                </div>
            </div>
        </div>

        <div class="field">
            <label style="float:left;">Comuna *</label>
            <div class="ui mini input">
                <select class="ui search dropdown" name="city_id" id="city" required>
                    @forelse($cities as $city)
                    <option value="{{ $city->id }}">{{ $city->label }}</option>
                    @empty
                    @endforelse
                </select>
            </div>
        </div>

        <div class="field">
            <label style="float:left;">Empresa</label>
            <div class="ui  mini input">
                <select class="dropdown" name="company_id" id="company" required>
                    @forelse($companies as $company)
                    <option value="{{ $company->id }}">{{$company->company_name}}</option>
                    @empty
                    @endforelse
                    <option value="all">Sin / Empresa</option>
                </select>
            </div>
        </div>
        <div class="field">
                <div class="ui checkbox" style="float:left;">
                  <input type="checkbox" tabindex="0" class="hidden" required>
                  <label style="float:left;">Acepto los <a href="/terms_conditions" data-lity>terminos y condiciones de uso</a></label>
                </div>
        </div>
        <br>
        <br>
        <input type="submit" class="ui fluid green submit button" value="Registrarme">
     </div>

      @if(count($errors))
      <div class="ui form error" style="text-align: left;">
          <div class="ui error message">
                <div class="header">Errores</div>
                @foreach($errors->all() as $error)
                <p>{{ $error}}</p>
                @endforeach
          </div>
      </div>
      @endif

      @if(session()->has('user_reg_success'))
      <div class="ui form success" style="text-align: left;">
          <div class="ui success message" >
                <div class="header">Registro Exitoso!<br> solo nos falta lo ultimo :)</div>
                <p> Para activar tu cuenta debes realizar lo siguiente:</p>
                <ol>
                    <li>Ir a tu casilla de Correo : <strong> </strong></li>
                    <li>Revisar tu bandeja de Entrada o Spam </li>
                    <li>Ir al mensaje de Bienvenida T-MIDO</li>
                    <li>Click en "Validar Cuenta" o pegar link en Navegador</li>
                    <li>Ya puedes entrar a T-mido - <a href="/login">LOGIN</a></li>
                </ol>
          </div>
      </div>
      @endif
    </form>
  </div>
</div>

<style type="text/css">
    body > .grid {
      height: 100%;
    }
    .image {
      margin-top: -100px;
    }
    .column {
      max-width: 450px;
    }
    .dropdown{
        width:100%;
    }
  </style>
  <script type="text/javascript">

 var format = "dd/mm/yyyy";
var match = new RegExp(format.replace(/(\w+)\W(\w+)\W(\w+)/, "^\\s*($1)\\W*($2)?\\W*($3)?([0-9]*).*").replace(/d|m|y/g, "\\d"));
var replace = "$1/$2/$3$4".replace(/\//g, format.match(/\W/));

function doFormat(target)
{
    target.value = target.value.replace(/(^|\W)(?=\d\W)/g, "$10").replace(match, replace).replace(/(\W)+/g, "$1");            // remove repeats
}

$("input[name='birthdate']:first").keyup(function(e) {
if(!e.ctrlKey && !e.metaKey && (e.keyCode == 32 || e.keyCode > 46))
doFormat(e.target)
});

      $(document).ready(function() {

      });
  </script>
@endsection
