@extends('layouts.app')
@section('content')
<div class="ui client_ container">
    <div class="ui segment " style="background-color:#1B1C1D;color:white;">
        <div class="ui inverted secondary menu">
            <a href="/company/{{ $company->id}}" style="">
                @if($company->photo)
                <img class="logo_company ui rounded image" src="/img/companies/{{ $company->photo}}" style="max-height:55px;">
                @else
                <img class="logo_company ui rounded image" src="/img/company_default.png" style="max-height:55px;">
                @endif
            </a>
            <span style="font-size:13px;font-weight:bold;line-height: 60px;margin-left: 30px;">{{ $company->company_name}}</span>
        </div>
    </div>
    <div class="ui two column stackable grid">
        <div class="column">
            <h4 class="ui horizontal divider header">
                <i class="tag icon"></i>
                Evaluaciones
            </h4>
            <div class="ui three cards">
                @forelse($single_evaluations as $single)
                @if($single->profiles->first() != null)
                    @if($single->status == 'enabled')
                    <a class="card tooltip" href="/evaluation/{{ $single->id }}" data-content="{{ $single->title }}">
                        <div class="extra content">
                            <span class="title_evaluation_cell">{{ substr($single->title,0,20) }}...</span>
                        </div>
                        <div class="image">
                            <img src="{{ $single->profiles->first()->photo }}">
                        </div>
                        <div class="content">
                            <div class="title_profile_cell">
                                {{ $single->profiles->first()->title }}
                            </div>
                        </div>
                        <div class="extra content">
                            @if($single->type == 'star_value')
                            <div class="ui column grid">
                                <div class="computer only sixteen wide column">
                                    <div class="ui star rating" id="prom_rating" data-rating="{{ round($single->getResponsesSimpleEvaluation()) }}" data-max-rating="5"></div>
                                    <span class="like_text_cell">{{ round($single->getResponsesSimpleEvaluation(),1) }}</span>
                                </div>
                                <div class="tablet only mobile only sixteen wide column">
                                    <div class="ui star rating" data-rating="1"><i class="icon active"></i></div>
                                    <span class="like_text_cell"> {{ $single->getResponsesSimpleEvaluation() }} </span>
                                </div>
                            </div>
                            @endif
                            @if($single->type == 'like_dlike')
                            <i class="thumbs green up icon"></i> <span class="like_text_cell"> {{ explode(',',$single->getResponsesSimpleEvaluation())[0] }} </span> |
                            <i class="thumbs red down icon"></i> <span class="like_text_cell"> {{ explode(',',$single->getResponsesSimpleEvaluation())[1] }} </span>
                            @endif
                            @if($single->type == 'yes_no')
                            <span class="yes_not_text_cell">SI</span> <span class="like_text_cell"> {{ explode(',',$single->getResponsesSimpleEvaluation())[0] }} </span> |
                            <span class="yes_not_text_cell">NO</span> <span class="like_text_cell"> {{ explode(',',$single->getResponsesSimpleEvaluation())[1] }}</span>
                            @endif
                        </div>
                    </a>
                    @endif
                @endif

                @empty
                <span></span>
                @endforelse
            </div>
        </div>
        <div class="column">
            <h4 class="ui horizontal divider header">
                <i class="tag icon"></i>
                Competencias y Votaciones
            </h4>
            @forelse($multiple_evaluations as $multiple)

            @if($multiple->status == 'enabled')

                @if($multiple->type == 'vote')
                <div class="multiple">
                    <div class="ui segment">
                        <div class="ui grid">
                            <div class="ten wide column"><h5>{{strtoupper($multiple->title)}}</h5></div>
                            <div class="six wide column">
                                <a class="ui green mini button right floated" href="/evaluation/{{ $multiple->id }}">Ver & Evaluar</a>
                            </div>
                        </div>
                        <div class="ui six special cards ">
                            @forelse( $multiple->sortProfilesEvaluation() as $profile)
                            <a class="card">
                                <div class="blurring dimmable image">
                                    <div class="ui dimmer">
                                        <div class="content">
                                            <div class="center">
                                                {{ $profile->getVotes($multiple->id) }} <br>
                                                votos
                                            </div>
                                        </div>
                                    </div>
                                    <img src="{{$profile->photo}}">
                                </div>
                            </a>
                            @empty

                                <label class="ui default label"> Sin perfiles asignados aun...</label>

                            @endforelse
                        </div>
                    </div>
                </div>
                <br>
                @else
                <div class="multiple">
                    <div class="ui segment">
                        <div class="ui grid">
                            <div class="ten wide column"><h5>{{strtoupper($multiple->title)}}</h5></div>
                            <div class="six wide column">
                                has click en el {{ $multiple->suffix }} para evaluar
                            </div>
                        </div>
                        <div class="ui six special cards ">
                            @forelse( $multiple->profiles as $profile)
                            <a class="card" href="/evaluation/{{ $multiple->id }}/profile/{{ $profile->id }}" >
                                <div class="blurring dimmable image">
                                    <div class="ui dimmer">
                                        <div class="content">
                                            <div class="center">
                                                <br>
                                                Evaluar!
                                            </div>
                                        </div>
                                    </div>
                                    <img src="{{$profile->photo}}">
                                </div>
                            </a>
                            @empty
                            <label class="ui default label"> Sin perfiles asignados aun...</label>
                            @endforelse
                        </div>
                    </div>
                </div>
                <br>
                @endif

            @endif

            @empty

            @endforelse
        </div>
    </div>
    <script type="text/javascript">
    $("body").floatingSocialShare({
           place: "top-left", // alternatively content-left, content-right, top-right
           counter: true, // set to false for hiding the counters of buttons
           twitter_counter: false, // Twitter API does not provide counters without API key, register to https://opensharecount.com/
           buttons: [ // all of the currently available social buttons
             "mail", "facebook", "google-plus","pinterest","twitter", "whatsapp"
           ],
           title: document.title, // your title, default is current page's title
           url: window.location.href,  // your url, default is current page's url
           text: { // the title of tags
             'default': 'Compartir con ',
             'facebook': 'Compartir con facebook',
             'google-plus': 'Compartir con g+'
           },
           text_title_case: $('meta[name="title"]').attr("content"), // if set true, then will convert share texts to title case like Share With G+
           description: $('meta[name="description"]').attr("content"), // your description, default is current page's description
           media: $('meta[property="og:image"]').attr("content"), // pinterest media
           popup: true, // open links in popup
           popup_width: 400, // the sharer popup width, default is 400px
           popup_height: 300 // the sharer popup height, default is 300px
     });
    </script>
    @include('layouts.footer')
</div>
@endsection
