@extends('layouts.app')
@section('content')
<div class="ui client_ container">
    <div class="ui segment " style="background-color:#1B1C1D;color:white;">
        <div class="ui inverted secondary menu">
            <a href="/company/{{ $company->id}}" style="">
                @if($company->photo)
                <img class="logo_company ui rounded image" src="/img/companies/{{ $company->photo}}" style="max-height:55px;">
                @else
                <img class="logo_company ui rounded image" src="/img/company_default.png" style="max-height:55px;">
                @endif
            </a>
            <span style="font-size:13px;font-weight:bold;line-height: 60px;margin-left: 30px;">{{ $company->company_name}}</span>
        </div>
    </div>
    <div class="ui breadcrumb segment">
      <a href="/company/{{ $company->id}}">Inicio</a>
      <i class="right angle icon divider"></i>
      <div class="active section">Votacion</div>
    </div>
    <br><br>
    <div class="ui two column stackable grid">
        <div class="ten wide column">
            <div class="ui  twelve wide column grid" >
                <div class="fiveteen wide column">
                    <h5>{{strtoupper($multiple->title)}}</h5>

                    <a class="ui floated right mini circular icon green button pop " href="#profile" data-content="ir a Evaluacion" data-variation="inverted" ><i class="checkmark box  icon"></i></a>
                    <a class="ui floated right mini circular icon green button pop " href="#results" data-content="ir a Resultados" data-variation="inverted" ><i class="pie chart icon"></i></a>
                    <div class="ui divider"></div>

                    {{ $multiple->description }} |
                    <strong>Escoge tu Opcion y vota!</strong>
                    <br>
                    <div class="ui grid">
                        <div class="column" style="margin-top:5px;">
                            @if(session()->has('nologin'))
                            <div class="ui negative message">
                                <i class="close icon"></i>
                                <div class="header">
                                    Lo sentimos!
                                </div>
                                <p>{{ session()->get('nologin') }} <a href="/login?url={{ URL::current() }}"> Entrar </a>
                                </p>
                            </div>
                            @endif
                            @if(session()->has('okeval'))
                            <div class="ui negative message">
                                <i class="close icon"></i>
                                <div class="header"> Lo sentimos! </div>
                                <p>{{ session()->get('okeval')}} </p>
                            </div>
                            @endif
                            @if(session()->has('noactive'))
                            <div class="ui negative message">
                                <i class="close icon"></i>
                                <div class="header"> Lo sentimos! </div>
                                <p>{{ session()->get('noactive')}} </p>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="ui divider"></div>
                    <a name="profile" class="anchor"></a>
                    <div class="ui five doubling cards ">
                        @forelse($profiles as $profile)

                            @if( $multiple->end_date > Carbon\Carbon::now() )
                            <a class="ui card popup-card"  data-html='<span class="ui center aligned" ><strong>{{$profile->title}}</strong><br><a href="{{ $profile->url_video }}" class="ui blue mini label" title="Ver Video" data-lity><i class="record icon"></i> Video</a>
                                <a class="ui mini blue label " href="{{ $profile->url_document }}" title="Doc/Info" data-lity><i class="fa fa-info-circle" ></i> Informacion </a><br><hr><form  action="/vote_register" method="post">{{ csrf_field() }}<input type="hidden" name="evaluation_id" value="{{ $multiple->id }}"><input type="hidden" name="profile_id" value="{{ $profile->id }}"><button class="ui fluid mini green  button" title="VOTAR!!" type="submit" >Votar</button> </form> </span>'  id="{{ $profile->id }}" href="javascript:selectCard({{ $profile->id }});">

                            @else

                            <a class="ui card popup-card"  data-html='<span class="ui center aligned" ><strong>{{$profile->title}}</strong><br><a href="{{ $profile->url_video }}" class="ui blue mini label" title="Ver Video" data-lity><i class="record icon"></i> Video</a>
                                <a class="ui mini blue label " href="{{ $profile->url_document }}" title="Doc/Info" data-lity><i class="fa fa-info-circle" ></i> Informacion </a><br><hr><button class="ui fluid mini red  button" title="Cerrada!!" type="submit" >Evaluacion Cerrada</button></span>'  id="{{ $profile->id }}" href="javascript:selectCard({{ $profile->id }});">

                            @endif

                            <div class="content" style="background-color:black;color:white;">
                                <img  src="/img/position_vote.png"  style="width:15px;">
                                <strong><small>{{ $loop->index + 1}}° Lugar</small></strong>
                            </div>
                            <div class="image">
                                <img src="{{$profile->photo}}">
                            </div>
                            <div class="content" style="background-color:black;color:white;">
                                <small>{{  $profile->votes }} Votos</small>
                            </div>
                        </a>
                        @empty
                        <span></span>
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="ui ten column">
                <div class="ui divider"></div>
                <div class="ui two column stackable grid">
                    <div class="column">
                        <a class="ui small  blue label pop" data-content="Visitas">
                            <i class="user icon"></i>
                            {{ $multiple->visit != null ? $multiple->visit : 0 }} <br>
                        </a>
                        <a class="ui small green label pop" data-content="Total Votos">
                            <i class="checkmark box  icon"></i>
                            {{ $user_eval_send != null ? $user_eval_send : 0}} <br>
                        </a>
                    </div>
                    <div class="column">
                        <div class="ui grid">
                            <div class="fourteen wide column">
                                <strong><small>Cierra en: </small></strong>
                                <span id="countdown">
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ui divider"></div>
            </div>
            <br>
        </div>
        <a name="results" class="anchor"></a>
        <div class="five wide column" >
            <div class="ui grid">
                <canvas id="barChart"></canvas>
                <small style="color:gray;"><strong>NOTA:</strong> Para ver el porcentaje de cada Item/Competidor pasa el puntero por encima del color respectivo en el grafico.</small>
            </div>
        </div>
    </div>
    @include('layouts.footer')
    <div class="ui modal">
        <i class="close icon"></i>
        <div class="content">
            <div class="ui embed"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
initCountdown('{{ $multiple->end_date }}');

$("body").floatingSocialShare({
       place: "top-left", // alternatively content-left, content-right, top-right
       counter: true, // set to false for hiding the counters of buttons
       twitter_counter: false, // Twitter API does not provide counters without API key, register to https://opensharecount.com/
       buttons: [ // all of the currently available social buttons
         "mail", "facebook", "google-plus","pinterest","twitter", "whatsapp"
       ],
       title: document.title, // your title, default is current page's title
       url: window.location.href,  // your url, default is current page's url
       text: { // the title of tags
         'default': 'Compartir con ',
         'facebook': 'Compartir con facebook',
         'google-plus': 'Compartir con g+'
       },
       text_title_case: $('meta[name="title"]').attr("content"), // if set true, then will convert share texts to title case like Share With G+
       description: $('meta[name="description"]').attr("content"), // your description, default is current page's description
       media: $('meta[property="og:image"]').attr("content"), // pinterest media
       popup: true, // open links in popup
       popup_width: 400, // the sharer popup width, default is 400px
       popup_height: 300 // the sharer popup height, default is 300px
 });
</script>
@include('layouts.voting_charts_js')
@endsection
