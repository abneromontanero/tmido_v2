<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="/css/welcome.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.10/semantic.min.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" ></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.10/semantic.min.js" ></script>
        <style media="screen">
            body{
                background-color: black;
            }
            .logo{
                width: 100%;
                padding: 20px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}"    class="ui mini button" >Mi Cuenta</a>
                    @else
                        <a href="{{ url('/login') }}" class="ui mini primary button" >Login</a>
                        &nbsp;<a href="{{ url('/register') }}" class="ui mini green button">Registro</a>
                    @endif
                </div>
            @endif
            <div class="content">
                <div class="title m-b-md">
                    <img  class="logo" src="img/logo_tmido_blanco.png">
                </div>
                <div class="links">
                @forelse ($companies as $company)

                        <a href="/company/{{ $company->id }}" style="float:left;">
                            <div style="max-width:150px;text-align:center;">
                                @if($company->photo)
                                <img src="/img/companies/{{ $company->photo }}" class="ui rounded image" style="max-height:50px;text-align:center;margin:auto;"><br>
                                <span class="ui small label grey" style="width:auto;max-height:50px;">{{$company->company_name}}</span>
                                @else
                                <img src="/img/company_default.png" class="ui rounded image" style="max-height:50px;margin:auto;"><br>
                                <span class="ui small label grey" style="width:auto;max-height:50px;">{{$company->company_name}}</span>
                                @endif
                            </div>
                        </a>

                @empty
                @endforelse
                </div>
            </div>
        </div>
    </body>
</html>
