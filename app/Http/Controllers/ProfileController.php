<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{

    public function index()
    {
        return view('company.profiles.index')
        ->with('profiles', \App\Profile::where('user_id', \Auth::user()->id)->get());
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //$img = $request->photo;
	    //$img = str_replace('data:image/png;base64,', '', $img);
	    //$img = str_replace(' ', '+', $img);
	    //$data = base64_decode($img);


        //$photo_name = $request->title.'_'.\Auth::user()->id.'.png';
        //$data->move( base_path() . '/public/img/profiles', $photo_name);

        $photo_name = '/public/img/profiles/'.$request->title.'_'.\Auth::user()->id.'.png';
        //$success = file_put_contents($file, $data);

        $data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->photo));
        file_put_contents(public_path('img/profiles/').$request->title.'_'.\Auth::user()->id.'.png', $data);

        $request->request->add(['photo_path' => $photo_name]);
        $request->request->add(['user_id' => \Auth::user()->id]);
        \App\Profile::create($request->all());
        return redirect('/profiles_company');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $profile = \App\Profile::find($id);
        if($profile->user_id == \Auth::user()->id)
        {
            return view('company.profiles.edit')
            ->with('profile', $profile);
        }
        else
        {
            return redirect()->back();
        }

    }


    public function update(Request $request, $id)
    {
        $profile = \App\Profile::find($id);
        if($profile->user_id == \Auth::user()->id)
        {
            $request->request->add(['user_id' => \Auth::user()->id]);
            $profile->update($request->all());
            return redirect('/profiles_company');
        }
        else
        {
            return redirect()->back();
        }


    }



    public function destroy($id)
    {
        \App\Profile::destroy($id);
        return redirect('/profiles_company');
    }
}
