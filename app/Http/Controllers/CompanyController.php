<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function getStatistics()
    {
        return view('company.statistics.index')
            ->with('evaluations', \App\User::find(\Auth::user()->id)->evaluations )
            ->with('profiles', \App\User::find(\Auth::user()->id)->profiles )
            ->with('users', \App\User::find(\Auth::user()->id)->users);
    }
    //----------User Functions--------------------------------------------------

    public function getUsers()
    {
        return view('company.users.index')
        ->with('users', \App\User::find(\Auth::user()->id)->users);
    }

    public function saveUser(Request $request)
    {
        if($request->company_photo)
        {
          $photo_name = $request->file('company_photo')->getClientOriginalName();
          $request->file('company_photo')->move( base_path() . '/public/img/users', $photo_name);
        }
        else
        {
            $photo_name = null;
        }

        \App\User::create([
        'first_name' => $request->first_name,
        'last_name' => $request->last_name,
        'email' => $request->email,
        'role' =>'user',
        'photo' => $photo_name,
        'password' => bcrypt($request->password),
        'user_id' => \Auth::user()->id,
        'status' => 'active'
        ]);

        return redirect('/users_company');
    }

    public function editUser($id)
    {
        return view('company.users.edit')->with('user', \App\User::find($id));
    }

    public function updateUser(Request $request)
    {
        $user = \App\User::find($request->user_id);
        $photo_name = $user->photo;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;

        if($request->password != null)
        {
            $user->password = $request->password;
        }
        $user->photo = $photo_name;
        $user->save();
        return redirect('/users_company');
    }

    public function destroyUser($id)
    {
        \App\User::destroy($id);
        return redirect('/users_company');
    }

    public function changeStatusUser($id)
    {
        $user = \App\User::find($id);
        if($user->status == 'active')
        {
            $user->status = 'inactive';
        }
        else
        {
            $user->status = 'active';
        }
        $user->save();
        return redirect('/users_company');
    }

    //------------- # ----------------------------------------------------------


    public function getComments()
    {
        return view('company.comments');
    }

    public function getAccount()
    {
        return view('company.account.index')->with('user',\Auth::user());
    }

    public function updateAccount(Request $request)
    {
        $company = \App\User::find($request->company_id);
        if($request->company_photo)
        {
          $photo_name = $request->file('company_photo')->getClientOriginalName();
          $request->file('company_photo')->move( base_path() . '/public/img/companies', $photo_name);
        }
        else
        {
            $photo_name = $company->photo;
        }
        $company->first_name = $request->first_name;
        $company->last_name = $request->last_name;
        $company->email = $request->email;
        $company->url_name = strtolower(preg_replace('[\s+]','', $request->url_name));
        if($request->password != null)
        {
            $company->password = bcrypt($request->password);
        }
        $company->photo = $photo_name;
        $company->company_name = $request->url_name;
        $company->private_site = $request->private_site;
        $company->save();

        return redirect('/account_company');
    }
}
