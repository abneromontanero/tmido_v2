<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/home';

    public function __construct(Request $request)
    {

        if($request->url)
        {
            $this->redirectTo = $request->url;
        }

        $this->middleware('guest', ['except' => 'logout']);
    }
}
