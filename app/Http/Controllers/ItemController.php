<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id_evaluation)
    {
        return view('company.items.index')
            ->with('items',\App\Evaluation::find($id_evaluation)->items)
            ->with('evaluation',\App\Evaluation::find($id_evaluation));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        if(\App\Evaluation::find($id)->items->count() < 5)
        {
            $request->request->add(['evaluation_id' => $id]);
            \App\Item::create($request->all());
        }

        return redirect('/eva_items/'.$id.'');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('company.items.edit')
            ->with('item',\App\Item::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = \App\Item::find($id);
        $item->update($request->all());
        return redirect('/eva_items/'.$request->evaluation_id.'');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id_evaluation = \App\Item::find($id)->evaluation->id;
        \App\Item::destroy($id);
        return redirect('/eva_items/'.$id_evaluation.'');
    }
}
