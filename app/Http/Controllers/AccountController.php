<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class AccountController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

    public function getUserAccount()
    {
        return view('user.index')
            ->with('cities', \App\City::all())
            ->with('user', \Auth::user());
    }

    public function updateUserAccount(Request $request)
    {

        $request->request->add(['birthdate' => Carbon::createFromFormat('d/m/Y', $request->birth)->toDateString()]);
        $user = \App\User::find($request->user_id);
        $user->update($request->all());
        return redirect('/user_account');
    }
}
