<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

/////////
class EnrollmentController extends Controller
{
    public function registerUser(Request $request)
    {
        $company = $request->company_id != 'all' ? $request->company_id : null;
        $this->validate($request, [
            'email' => 'required|email|unique:users,email',
            'first_name' => 'required|min:1',
            'password' => 'required|min:6',
        ]);

        $user =  \App\User::create([
            'first_name' => $request->first_name,
            'email' => $request->email,
            'gender' => $request->gender,
            'birthdate' => Carbon::createFromFormat('d/m/Y', $request->birthdate)->toDateString(),
            'city_id' => $request->city_id,
            'password' => bcrypt($request->password),
            'role' => 'user',
            'user_id'=> $company
          ]);

        if($user)
        {
            $token = str_random(40);
            $domain = request()->getHost();
            $enrollment =  \App\Enrollment::create([
                'token' => $token,
                'user_id'=> $user->id
              ]);

              \Mail::send('mails.user_register',['token'=>$token, 'email'=>$user->email , 'domain'=>$domain], function ($message) use ($user) {
                $message->from('noresponder@t-mido.cl', 'T-mido');
                $message->to($user->email)->subject('Bienvenido a T-mido!');
              });

            session()->flash('user_reg_success',$user);
            return redirect('/register');
        }

    }

    public function validateAccount($token)
    {
        $enrollment = \App\Enrollment::where('token',$token)->first();

        if($enrollment)
        {
            $user = \App\User::find($enrollment->user_id);
            $user->status = "active";
            $user->save();
            $enrollment->delete();

            session()->flash('account_active',$user);
            return redirect('/login');
        }
        else
        {
            session()->flash('account_inactive','inactivo');
            return redirect('/login');
        }

    }
}
