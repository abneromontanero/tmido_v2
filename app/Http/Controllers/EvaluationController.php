<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Date\Date;
use Carbon\Carbon;

class EvaluationController extends Controller
{

    public function index()
    {
        return view('company.evaluations.index')
        ->with('tags',\App\Tag::all())
        ->with('evaluations', \App\Evaluation::where('user_id', \Auth::user()->id)->get());
    }


    public function create()
    {

    }

    public function store(Request $request)
    {


        if($request->suffix)
        {
            $request->request->add(['suffix' => $request->suffix]);
        }
        else
        {
            $request->request->add(['suffix' => null ]);
        }
        $end_date  = Carbon::createFromFormat('d/m/Y H:i', $request->end_date_eval)->toDateTimeString();
        $request->request->add(['end_date' => $end_date]);
        $request->request->add(['user_id' => \Auth::user()->id]);

        if($request->type == 'vote')
        {
            $request->request->add(['mode' => 'multiple']);
        }
        else
        {
            $request->request->add(['mode' => $request->mode_eva]);
        }

        $evaluation = \App\Evaluation::create($request->only('title','description','type','end_date','mode','score','status','auth_view','suffix','user_id'));

        if($request->type != 'vote')
        {
            for($i=0; $i < $request->count_items; $i++)
             {
                $a = $i+1;
                $item = new \App\Item;
                $item->description = $request->input('item_'.$a);
                $item->evaluation_id = $evaluation->id;
                $item->save();
             }
        }

        foreach ($request->tags as $tag)
        {
            $evaluation->tags()->attach(\App\Tag::firstOrCreate(['name' => $tag]));
        }

        return redirect('/evaluations_company');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        return view('company.evaluations.edit')->with('evaluation',\App\Evaluation::find($id));
    }


    public function update(Request $request, $id)
    {

        $evaluation = \App\Evaluation::find($id);
        if($request->suffix)
        {
            $request->request->add(['suffix' => $request->suffix]);
        }
        else
        {
            $request->request->add(['suffix' => null ]);
        }
        $end_date  = Carbon::createFromFormat('d/m/Y H:i', $request->end_date_eval)->toDateTimeString();
        $request->request->add(['end_date' => $end_date]);
        $request->request->add(['user_id' => \Auth::user()->id]);

        if($request->type == 'vote')
        {
            $request->request->add(['mode' => 'multiple']);
        }
        else
        {
            $request->request->add(['mode' => $request->mode_eva]);
        }

        if($evaluation->type != 'vote' && $request->type == 'vote')
        {
            foreach ($evaluation->items as $item)
             {
                $item->delete();
             }
        }

        if($evaluation->type == 'vote' && $request->type != 'vote')
        {
            if($evaluation->profiles)
            {
                $evaluation->profiles()->detach();
            }

            if ($evaluation->items) {
                foreach ($evaluation->items as $item)
                 {
                    $item->delete();
                 }
            }
        }

        $evaluation->update($request->only('title','description','type','end_date','mode','score','status','auth_view','suffix','user_id'));


        return redirect('/evaluations_company');
    }

    public function assingProfile($id)
    {
        $evaluation = \App\Evaluation::find($id);
        $profiles_assign = $evaluation->profiles;
        $profiles = \App\Profile::all();
        $diff = $profiles->diff($profiles_assign);
        $profiles_no_assign = $diff->all();

        return view('company.evaluations.assign')
            ->with('evaluation',$evaluation)
            ->with('profiles_assign',$profiles_assign)
            ->with('profiles_no_assign',$profiles_no_assign);
    }

    public function saveAssingProfile(Request $request)
    {


        $evaluation = \App\Evaluation::find($request->evaluation_id);
        if($evaluation->profiles)
        {
            $evaluation->profiles()->detach();
        }
        $evaluation->profiles()->attach($request->profiles);
        return redirect('/evaluations_company');
    }

    public function destroy($id)
    {
        \App\Evaluation::destroy($id);
        return redirect('/evaluations_company');
    }
}
