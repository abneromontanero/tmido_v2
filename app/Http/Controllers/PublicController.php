<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function evaluationsCompany($id)
    {
        $company = \App\User::find($id);
        if ($company->private_site == 1)
        {
          if(\Auth::guest())
          {
              return redirect('/login');
          }
          elseif(\Auth::user()->user_id != $company->id)
          {
              return redirect('/login');
          }
          else
          {

              return view('public.directory')
                ->with('company',$company)
                ->with('single_evaluations',$company->evaluations->where('mode','single'))
                ->with('multiple_evaluations',$company->evaluations->where('mode','multiple'));
          }
        }
        else
        {
            return view('public.directory')
              ->with('company',$company)
              ->with('single_evaluations',$company->evaluations->where('mode','single'))
              ->with('multiple_evaluations',$company->evaluations->where('mode','multiple'));
        }

    }

    public function getSpecialMultiple( $evaluation_id , $profile_id )
    {
        $letter_list = ['A','B','C','D','E','F','G','H','I'];
        $evaluation = \App\Evaluation::find($evaluation_id);
        $profile = \App\Profile::find($profile_id);
        $user_eval_send = $evaluation->response_counter;
        $company = $evaluation->user;
        $evaluation->addVisit();
        $avgs = [];

        if($evaluation->type == 'star_value')
        {
          foreach ($evaluation->items as $item)
          {
            array_push($avgs, $item->responses->where('profile_id',$profile_id)->avg('value'));
          }
          if(count($avgs)!= 0)
          {
            $average = array_sum($avgs)/count($avgs);
          }
          else
          {
            $average = 0;
          }

        }
        elseif($evaluation->type == 'like_dlike')
        {
          $likes = 0;
          $dlikes = 0;
          foreach ($evaluation->items as $item)
          {
            $likes = $likes + $item->responses->where('profile_id',$profile_id)->where('value','like')->count();
            $dlikes = $dlikes + $item->responses->where('profile_id',$profile_id)->where('value','dlike')->count();
          }
          $average = $likes.','.$dlikes;
        }
        elseif($evaluation->type == 'yes_no')
        {
          $yes = 0;
          $no = 0;
          foreach ($evaluation->items as $item)
          {
            $yes = $yes + $item->responses->where('profile_id',$profile_id)->where('value','yes')->count();
            $no = $no + $item->responses->where('profile_id',$profile_id)->where('value','no')->count();
          }
          $average = $yes.','.$no;
        }

        return view('public.multiple_special')
            ->with('total_avg',$average)
            ->with('user_eval_send',$user_eval_send)
            ->with('letter_list',$letter_list)
            ->with('company',$company)
            ->with('profile',$profile)
            ->with('eval',$evaluation)
            ->with('single', $evaluation );

    }

    public function getEvaluation($id)
    {
        $letter_list = ['A','B','C','D','E','F','G','H','I'];
        $evaluation = \App\Evaluation::find($id);
        $profile = $evaluation->profiles->first();

        $user_eval_send = $evaluation->response_counter;

        $company = $evaluation->user;
        $evaluation->addVisit();

      //AVG total
        $avgs = [];
        if($evaluation->mode == 'single' && $evaluation->type == 'star_value')
        {
          foreach ($evaluation->items as $item)
          {
            array_push($avgs, $item->responses->avg('value'));
          }
          $average = array_sum($avgs)/count($avgs);
        }
        elseif($evaluation->mode == 'single' && $evaluation->type == 'like_dlike')
        {
          $likes = 0;
          $dlikes = 0;
          foreach ($evaluation->items as $item)
          {
            $likes = $likes + $item->responses->where('value','like')->count();
            $dlikes = $dlikes + $item->responses->where('value','dlike')->count();
          }
          $average = $likes.','.$dlikes;
        }
        elseif($evaluation->mode == 'single' && $evaluation->type == 'yes_no')
        {
          $yes = 0;
          $no = 0;
          foreach ($evaluation->items as $item)
          {
            $yes = $yes + $item->responses->where('value','yes')->count();
            $no = $no + $item->responses->where('value','no')->count();
          }
          $average = $yes.','.$no;
        }

        //caso de single
        if($evaluation->mode == 'single'){
          return view('public.evaluation')
              ->with('total_avg',$average)
              ->with('user_eval_send',$user_eval_send)
              ->with('letter_list',$letter_list)
              ->with('company',$company)
              ->with('profile',$profile)
              ->with('eval',\App\Evaluation::find($id))
              ->with('single', \App\Evaluation::find($id));
        }
        else
        {
         $profiles = $evaluation->sortProfilesEvaluation();
          return view('public.voting')
              ->with('company',$company)
              ->with('user_eval_send',$user_eval_send)
              ->with('profiles',$profiles)
              ->with('eval',\App\Evaluation::find($id))
              ->with('multiple', \App\Evaluation::find($id));

        }

    }

    public function saveEvaluation(Request $request)
    {
     $scores = json_decode($request->scores);

      if(\Auth::guest())
      {
          session()->flash('nologin','Debes logearte para evaluar');
          return redirect()->back();
      }
      else
      {

        if(\Auth::user()->status == "active")
        {

            if( \App\Evaluation::find($request->evaluation_id)->type != 'vote' && \App\Evaluation::find($request->evaluation_id)->mode == 'multiple')
            {
                 $verify_eval0 = \App\Response::where('user_id',\Auth::user()->id)->where('profile_id',$request->profile_id)->where('evaluation_id',$request->evaluation_id)->get();
                 if($verify_eval0->count() == 0)
                 {
                     foreach ($scores as $score_uni) {
                       $response =   new \App\Response;
                       $response->value = $score_uni->score;
                       $response->letter = $score_uni->letter;
                       $response->item_id = $score_uni->id;
                       $response->profile_id = $score_uni->profile;
                       $response->evaluation_id = $request->evaluation_id;
                       $response->user_id = \Auth::user()->id;
                       $response->save();
                     }
                     $evaluation = \App\Evaluation::find($request->evaluation_id);
                     $evaluation->addResponse();
                     return redirect()->to(app('url')->previous(). '#results');
                 }
                 else
                 {
                     session()->flash('okeval','Lo sentimos :( ya evaluaste aqui!');
                     return redirect()->back();
                 }
            }
            else
            {
                $verify_eval = \App\Response::where('user_id',\Auth::user()->id)->where('evaluation_id',$request->evaluation_id)->get();
                if($verify_eval->count() == 0)
                {
                    foreach ($scores as $score_uni) {
                      $response =   new \App\Response;
                      $response->value = $score_uni->score;
                      $response->letter = $score_uni->letter;
                      $response->item_id = $score_uni->id;
                      $response->profile_id = $score_uni->profile;
                      $response->evaluation_id = $request->evaluation_id;
                      $response->user_id = \Auth::user()->id;
                      $response->save();
                    }
                    $evaluation = \App\Evaluation::find($request->evaluation_id);
                    $evaluation->addResponse();
                    return redirect()->to(app('url')->previous(). '#results');
                }
                else
                {
                    session()->flash('okeval','Lo sentimos :( ya evaluaste aqui!');
                    return redirect()->back();
                }
            }

        }
        else
        {
            session()->flash('noactive','Tu cuenta no esta activa para Evaluar :( !, contacta con el administrador de tu institucion para evaluar.');
            return redirect()->back();
        }

      }

    }

    public function voteRegister(Request $request)
    {
        if(\Auth::guest())
        {
            session()->flash('nologin','Debes logearte para Votar');
            return redirect()->back();
        }
        else
        {
            if(\Auth::user()->status == "active")
            {
                $verify_eval = \App\Response::where('user_id',\Auth::user()->id)->where('evaluation_id',$request->evaluation_id)->get();
                if($verify_eval->count() == 0)
                {
                    $response =   new \App\Response;
                    $response->value = 'vote';
                    $response->letter = 'A';
                    $response->profile_id = $request->profile_id;
                    $response->evaluation_id = $request->evaluation_id;
                    $response->user_id = \Auth::user()->id;
                    $response->save();
                    $evaluation = \App\Evaluation::find($request->evaluation_id);
                    $evaluation->addResponse();
                    return redirect()->to(app('url')->previous(). '#results');
                }
                else
                {
                    session()->flash('okeval','Lo sentimos :( ya Votaste!');
                    return redirect()->back();
                }

            }
            else
            {
                session()->flash('noactive','Tu cuenta no esta activa para Votar :( !, contacta con el administrador de tu institucion.');
                return redirect()->back();
            }
        }
    }

    public function getChartItem($item_id,$profile_id)
    {
        $item =  \App\Item::find($item_id);
        $responses = \App\Response::where('profile_id',$profile_id)->where('item_id',$item_id)->get();

        return view('layouts.charts_items')
                    ->with('item',$item)
                    ->with('responses',$responses);
    }
}
