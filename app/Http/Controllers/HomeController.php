<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if(\Auth::user()->isSuperAdmin())
        {
          return redirect('/statistics_admin');
        }
        elseif(\Auth::user()->isCompany())
        {
          return redirect('/statistics_company');
        }
        elseif(\Auth::user()->isUser())
        {
            return redirect('/company/'.\Auth::user()->user->id);
        }
        else
        {
          return view('home');
        }
    }
}
