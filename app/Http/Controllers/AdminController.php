<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//////
class AdminController extends Controller
{
    public function getStatistics()
    {
        return view('superadmin.statistics.index');
    }

    public function getCompanies()
    {
        return view('superadmin.companies.index')->with('companies',\App\User::where('role','company')->get());
    }

    public function saveCompany(Request $request)
    {

        if($request->company_photo)
        {
          $photo_name = $request->file('company_photo')->getClientOriginalName();
          $request->file('company_photo')->move( base_path() . '/public/img/companies', $photo_name);
        }
        else
        {
            $photo_name = null;
        }

        \App\User::create([
        'first_name' => $request->first_name,
        'last_name' => $request->last_name,
        'email' => $request->email,
        'role' =>'company',
        'url_name' =>strtolower(preg_replace('[\s+]','', $request->url_name)),
        'company_name' => $request->url_name,
        'private_site' => $request->private_site,
        'photo' => $photo_name,
        'password' => bcrypt($request->password)
        ]);

        return redirect('/companies_admin');
    }

    public function editCompany($id)
    {
        return view('superadmin.companies.edit')->with('company',\App\User::find($id));
    }

    public function updateCompany(Request $request)
    {
        $company = \App\User::find($request->company_id);
        if($request->company_photo)
        {
          $photo_name = $request->file('company_photo')->getClientOriginalName();
          $request->file('company_photo')->move( base_path() . '/public/img/companies', $photo_name);
        }
        else
        {
            $photo_name = $company->photo;
        }
        $company->first_name = $request->first_name;
        $company->last_name = $request->last_name;
        $company->email = $request->email;
        $company->url_name = strtolower(preg_replace('[\s+]','', $request->url_name));
        if($request->password != null)
        {
            $company->password = $request->password;
        }
        $company->photo = $photo_name;
        $company->company_name = $request->url_name;
        $company->private_site = $request->private_site;
        $company->save();
        return redirect('/companies_admin');
    }

    public function getAccounts()
    {
        return view('superadmin.accounts.index');
    }

    public function getPayments()
    {
        return view('superadmin.payments.index');
    }

    public function getContacts()
    {
        return view('superadmin.contacts.index');
    }
}
