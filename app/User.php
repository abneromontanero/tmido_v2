<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'first_name','last_name','url_name', 'email', 'password','role','company_name','photo','user_id','private_site','status','city_id','gender','birthdate'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function account() //Relationships -> Account
    {
        return $this->belongsTo('App\Account');
    }

    public function payments() //Relationships -> Payment
    {
        return $this->hasMany('App\Payment');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function comments() //Relationships -> Comment
    {
        return $this->hasMany('App\Comment');
    }

    public function responses() //Relationships -> Response
    {
        return $this->hasMany('App\Response');
    }

    public function evaluations() //Relationships -> Evaluation
    {
        return $this->hasMany('App\Evaluation');
    }

    public function profiles() //Relationships -> Evaluation
    {
        return $this->hasMany('App\Profile');
    }

    public function users() //Relationships -> get users
    {
        return $this->hasMany('App\User');
    }

    public function user() //Relationships -> get only one user
    {
        return $this->belongsTo('App\User');
    }

    public function isSuperAdmin()
    {
      return $this->role == 'superadmin' ? true : false;
    }

    public function isCompany()
    {
      return $this->role == 'company' ? true : false;
    }

    public function isUser()
    {
      return $this->role == 'user' ? true : false;
    }

    public function getSumScore()
    {
        $total_score = 0;
        $evaluation_in_responses = \DB::table('responses')->where('user_id',$this->id)->distinct()->get(['evaluation_id']);
        foreach ($evaluation_in_responses as $eval) {
            $evaluation = \App\Evaluation::find($eval->evaluation_id);
            $total_score = $total_score + $evaluation->score;
        }
        return $total_score;

    }
    public function getSumParticipation()
    {
        return  \DB::table('responses')->where('user_id',$this->id)->distinct()->get(['evaluation_id'])->count();
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
}
