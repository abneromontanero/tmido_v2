<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'title','description','photo_path','photo','url_video','url_document','user_id',
    ];
    public function evaluations() //Relationships -> Evaluation
    {
        return $this->belongsToMany('App\Evaluation')->withTimestamps();
    }

    public function user() //Relationships -> User
    {
        return $this->belongsTo('App\User');
    }

    public function responses() //Relationships -> Response
    {
        return $this->hasMany('App\Response');
    }

    public function getVotes($evaluation_id)
    {
      return  $this->responses->where('evaluation_id', $evaluation_id )->where('value','vote')->count();
    }
}
