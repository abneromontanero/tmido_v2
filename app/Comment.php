<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function user() //Relationships -> User
    {
        return $this->belongsTo('App\User');
    }
    
    public function evaluation() //Relationships -> Evaluation
    {
        return $this->belongsTo('App\Evaluation');
    }
}
