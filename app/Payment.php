<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function user() //Relationships -> User
    {
        return $this->belongsTo('App\User');
    }
}
