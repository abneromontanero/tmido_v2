<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'name'
    ];
    public function evaluations() //Relationships -> Evaluation
    {
        return $this->belongsToMany('App\Evaluation');
    }
}
