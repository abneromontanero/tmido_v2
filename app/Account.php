<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public function users() //Relationships -> User
    {
        return $this->hasMany('App\User');
    }
}
