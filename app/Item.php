<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

  protected $fillable = [
      'description','evaluation_id',
  ];

    public function responses() //Relationships -> Response
    {
        return $this->hasMany('App\Response');
    }

    public function evaluation() //Relationships -> Evaluation
    {
        return $this->belongsTo('App\Evaluation');
    }
}
