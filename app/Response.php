<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    public function user() //Relationships -> User
    {
        return $this->belongsTo('App\User');
    }

    public function item() //Relationships -> Item
    {
        return $this->belongsTo('App\Item');
    }

    public function evaluation() //Relationships -> Item
    {
        return $this->belongsTo('App\Evaluation');
    }

    public function profile() //Relationships -> Profile
    {
        return $this->belongsTo('App\Profile');
    }
}
