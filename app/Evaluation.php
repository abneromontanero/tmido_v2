<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
  protected $fillable = [
          'title','description','type', 'end_date', 'mode','score','status','auth_view','user_id','suffix'
      ];

    public function user() //Relationships -> User
    {
        return $this->belongsTo('App\User');
    }

    public function comments() //Relationships -> Comment
    {
        return $this->hasMany('App\Comment');
    }


    public function responses() //Relationships -> Response
    {
        return $this->hasMany('App\Response');
    }

    public function items() //Relationships -> Item
    {
        return $this->hasMany('App\Item');
    }

    public function profiles()
    {
         return $this->belongsToMany('App\Profile')->withTimestamps();
    }

    public function tags() //Relationships -> Tag
    {
        return $this->belongsToMany('App\Tag');
    }

    public function isAssigned()
    {
        return \DB::table('evaluation_profile')->where('evaluation_id', $this->id)->count() ? true : false;
    }

    public function addVisit()
    {
      $this->visit += 1;
      $this->save();
    }

    public function addResponse()
    {
      $this->response_counter+= 1;
      $this->save();
    }

    public function totalVotes()
    {
        if($this->mode == "multiple")
        {
            $total_votes = 0;
            $id_evaluation = $this->id;
            $this->profiles->map(function($profile) use ($id_evaluation){
              $votes = $profile->getVotes($id_evaluation);
              $total_votes = $total_votes + $votes;
          });
          return $total_votes;
        }
        else
        {
            return '';
        }
    }

    public function getResponsesSimpleEvaluation()
    {
        if($this->mode == 'single' && $this->type == 'star_value')
        {
            $avgs = [];
            foreach ($this->items as $item)
              {
                array_push($avgs, $item->responses->avg('value'));
              }
              $responses = array_sum($avgs)/count($avgs);
        }
        elseif($this->mode == 'single' && $this->type == 'like_dlike')
        {
              $likes = 0;
              $dlikes = 0;
              foreach ($this->items as $item)
              {
                $likes = $likes + $item->responses->where('value','like')->count();
                $dlikes = $dlikes + $item->responses->where('value','dlike')->count();
              }
              $responses = $likes.','.$dlikes;
        }
        elseif($this->mode == 'single' && $this->type == 'yes_no')
        {
              $yes = 0;
              $no = 0;
              foreach ($this->items as $item)
              {
                $yes = $yes + $item->responses->where('value','yes')->count();
                $no = $no + $item->responses->where('value','no')->count();
              }
              $responses = $yes.','.$no;
        }

        return $responses;
    }

    public function sortProfilesEvaluation()
    {
        $profiles = $this->profiles;
        $id = $this->id;
        $profiles->map(function($profile) use ($id){
            $profile['votes'] = $profile->getVotes($id);
            return $profile;
        });
        return $profiles->sortByDesc('votes');
    }
}
