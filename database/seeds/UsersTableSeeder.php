<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
          [

            'first_name' => 'Administrador',
            'last_name' => 'Super',
            'url_name' => null,
            'email' => 'admin@mail.com',
            'password' => bcrypt('adminadmin'),
            'role' => 'superadmin',
            'company_name' => null,
            'user_id'=>null,
            'status'=>'active'
          ],
          [

            'first_name' => 'Empresa',
            'last_name' => 'Empresa',
            'url_name' => 'empresa1',
            'email' => 'empresa@mail.com',
            'password' => bcrypt('adminadmin'),
            'role' => 'company',
            'company_name' => 'Empresa 1',
            'user_id'=>null,
            'status'=>'active'
          ],
          [

            'first_name' => 'Usuario1',
            'last_name' => 'Usuario',
            'url_name' => null,
            'email' => 'usuario1@mail.com',
            'password' => bcrypt('adminadmin'),
            'role' => 'user',
            'company_name' => null,
            'user_id'=> 2,
            'status'=>'active'
          ]

        ]);
    }
}
