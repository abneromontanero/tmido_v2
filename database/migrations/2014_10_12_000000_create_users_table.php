<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('status')->default('inactive'); //active / inactive
            $table->string('role');
            $table->string('url_name')->nullable();
            $table->string('company_name')->nullable();
            $table->boolean('private_site')->nullable();
            $table->string('gender')->nullable();
            $table->date('birthdate')->nullable();
            $table->string('country')->nullable();
            $table->string('region')->nullable();
            $table->integer('city_id')->unsigned()->nullable();//foreing
            $table->string('photo')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->integer('user_id')->unsigned()->nullable();//foreing in itself;
            $table->integer('account_id')->unsigned()->nullable();//foreing;
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
