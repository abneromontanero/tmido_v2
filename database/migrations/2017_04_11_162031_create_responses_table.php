<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('value'); // y/n,like/dlike,1-7,vote
            $table->string('letter')->nullable(); // A - G
            $table->integer('item_id')->unsigned()->nullable();//foreing;
            $table->integer('profile_id')->unsigned()->nullable();//foreing;
            $table->integer('evaluation_id')->unsigned()->nullable();//foreing;
            $table->integer('user_id')->unsigned()->nullable();//foreing;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responses');
    }
}
