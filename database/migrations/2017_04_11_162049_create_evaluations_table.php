<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type'); // like/dlike yes/no star_value vote..
            $table->date('end_date');
            $table->string('status')->nullable();//enable or disable
            $table->string('auth_view')->nullable(); //enable or disable
            $table->integer('visit')->unsigned()->nullable(); //count visit
            $table->integer('response_counter')->unsigned()->nullable(); //count reponses send
            $table->string('mode'); //multiple or single
            $table->string('title');
            $table->string('description')->nullable();
            $table->integer('score')->unsigned()->nullable(); //score for user participation
            $table->string('suffix')->nullable();
            $table->integer('user_id')->unsigned()->nullable();//foreing;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
}
