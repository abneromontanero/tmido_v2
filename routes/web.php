<?php
Route::get('/', function () {
    return view('welcome')
    ->with('companies',\App\User::where('role','company')->get());
});
Auth::routes();
Route::match(['get', 'post'], 'register', function(){
    return view('public.user_register')
    ->with('cities',\App\City::all())
    ->with('companies',\App\User::where('role','company')->get());
});
Route::get('/home', 'HomeController@index');
Route::get('/company/{id?}', 'PublicController@evaluationsCompany');
Route::post('/user_register','EnrollmentController@registerUser');
Route::get('/register_mail',function (){
    return view('mails.user_register');
});
Route::get('/evaluation/{id?}','PublicController@getEvaluation');
Route::get('/validate_account/token/{token?}','EnrollmentController@validateAccount');
Route::post('/save_score_items','PublicController@saveEvaluation');
Route::post('/vote_register','PublicController@voteRegister');
Route::get('/terms_conditions',  function(){
    return view('layouts.term_conditions');
});

Route::get('/evaluation/{evaluation?}/profile/{profile?}','PublicController@getSpecialMultiple');

Route::get('/chart_item/{item?}/profile/{profile?}','PublicController@getChartItem');

Route::group(['middleware' => 'auth'], function () {
      Route::group(['middleware' => 'superadmin'], function () {
          Route::get('/statistics_admin', 'AdminController@getStatistics');
          Route::get('/companies_admin', 'AdminController@getCompanies');
          Route::post('/companies_admin', 'AdminController@saveCompany');
          Route::get('/companies_admin/{id?}/edit','AdminController@editCompany');
          Route::post('/companies_admin/update', 'AdminController@updateCompany');
          Route::get('/accounts_admin', 'AdminController@getAccounts');
          Route::get('/payments_admin', 'AdminController@getPayments');
          Route::get('/contacts_admin', 'AdminController@getContacts');
      });
      Route::group(['middleware' => 'company'], function () {
          Route::get('/statistics_company', 'CompanyController@getStatistics');
          Route::get('/users_company', 'CompanyController@getUsers');
          Route::post('/users_company', 'CompanyController@saveUser');
          Route::get('/users_company/{id?}/edit','CompanyController@editUser');
          Route::post('/users_company/update', 'CompanyController@updateUser');
          Route::post('/users_company/{id?}/delete', 'CompanyController@destroyUser');
          Route::get('/profile_assign/{id?}','EvaluationController@assingProfile');
          Route::post('/profiles_assign','EvaluationController@saveAssingProfile');
          Route::resource('/evaluations_company', 'EvaluationController');
          Route::resource('/profiles_company', 'ProfileController');
          Route::get('/comments_company', 'CompanyController@getComments');
          Route::get('/account_company', 'CompanyController@getAccount');
          Route::post('/account_company/update', 'CompanyController@updateAccount');
          Route::get('/eva_items/{id_evaluation?}','ItemController@index');
          Route::post('/eva_items/{id_evaluation?}','ItemController@store');
          Route::get('/eva_items/{id?}/edit','ItemController@edit');
          Route::post('/update_items/{id?}','ItemController@update');
          Route::delete('/del_items/{id?}','ItemController@destroy');
          Route::get('/users_company/{id?}/change_status','CompanyController@changeStatusUser');
      });
      Route::group(['middleware' => 'user'], function () {

          Route::get('/user_account', 'AccountController@getUserAccount');
          Route::post('/user_account', 'AccountController@updateUserAccount');

      });

});
